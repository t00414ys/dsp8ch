#######
#
#  libvsensor files
#

LIBRARIES	= $(LIBDIR)/libalgorithm.a


##
#
##
SRCS	=	
SRCS	+=	libvsensor/src/activity/accl_move.c
SRCS	+=	libvsensor/src/activity/accl_pedo.c
SRCS	+=	libvsensor/src/activity/accl_step.c
SRCS	+=	libvsensor/src/activity/activity_detector.c
SRCS	+=	libvsensor/src/activity/activity_library.c
SRCS	+=	libvsensor/src/activity/calorie_sen.c
SRCS	+=	libvsensor/src/activity/health_manager.c
SRCS	+=	libvsensor/src/activity/motion_detector.c
SRCS	+=	libvsensor/src/activity/motion_library.c
SRCS	+=	libvsensor/src/activity/motion_sensing.c
SRCS	+=	libvsensor/src/activity/pedometer_library.c
SRCS	+=	libvsensor/src/activity/sleep_library.c
SRCS	+=	libvsensor/src/activity/step_detector.c
SRCS	+=	libvsensor/src/bikedetector/activity_recognition.c
SRCS	+=	libvsensor/src/bikedetector/bike_detector.c
SRCS	+=	libvsensor/src/falldown/accl_fall_down.c
SRCS	+=	libvsensor/src/gesture/gesture_core.c
SRCS	+=	libvsensor/src/gesture/glance_det.c
SRCS	+=	libvsensor/src/gesture/glance_stat.c
SRCS	+=	libvsensor/src/gesture/glance_status.c
SRCS	+=	libvsensor/src/gesture/pickup_det.c
SRCS	+=	libvsensor/src/gesture/pickup_detector.c
SRCS	+=	libvsensor/src/gesture/tilt_det.c
SRCS	+=	libvsensor/src/gesture/tilt_detector.c
SRCS	+=	libvsensor/src/gesture/wakeup_det.c
SRCS	+=	libvsensor/src/gesture/wakeup_detector.c
SRCS	+=	libvsensor/src/gesture/wrist_tilt_det.c
SRCS	+=	libvsensor/src/heartrate/heartrate_library.c
SRCS	+=	libvsensor/src/libs/bpl_sen.c
SRCS	+=	libvsensor/src/libs/bp_sen.c
SRCS	+=	libvsensor/src/libs/heartrate_sen.c
SRCS	+=	libvsensor/src/libs/pdr_geofencing.c
SRCS	+=	libvsensor/src/libs/pdr_sen.c
SRCS	+=	libvsensor/src/libs/proximity_detector.c
SRCS	+=	libvsensor/src/libs/wear_detector.c
SRCS	+=	libvsensor/src/prs/prs_stair.c
SRCS	+=	libvsensor/src/prs/stair_detector.c






##
#
##
INCS	=
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
INCS	+=	-I$(WORKSPACE_LOC)/$(STUBDIR)/include
endif
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libhub/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/activity
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/bikedetector
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/gesture
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/heartrate
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/prs
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/libs
INCS	+=	-I$(WORKSPACE_LOC)/libbase/frizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libcommon/inc
INCS	+=	-I$(WORKSPACE_LOC)/target/libcommon/inc
