/*!******************************************************************************
 * @file  frizz_util.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_UTIL_H__
#define __FRIZZ_UTIL_H__

#define		NELEMENT(a)					(sizeof(a)/sizeof(a[0]))

#if defined(__cplusplus)
#define EXTERN_C_START		extern "C" {
#define EXTERN_C_END		}
#define EXTERN_C			extern	"C"
#else
#define EXTERN_C_START
#define EXTERN_C_END
#define EXTERN_C			extern
#endif


#if	!defined(USE_QUART_USER_DEBUG)
//#define	USE_QUART_USER_DEBUG
#endif

#if	defined(RUN_ON_XPLORER)
EXTERN_C void	tiny_printf( char* fmt, ... );
#define	printf			tiny_printf
#elif defined(RUN_ON_PC)
#include <stdio.h>
#endif

//
//
#if	defined(USE_QUART_USER_DEBUG)
EXTERN_C void quart_out_raw( const char* form, ... );
#if	defined(RUN_ON_XPLORER)
#define		DEBUG_PRINT					printf
#else
#define		DEBUG_PRINT					quart_out_raw
#endif
#else
#define		DEBUG_PRINT(...)
#endif

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////
//
// PC(linux) Emulation Definition
//
#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)

#if defined(RUN_ON_XPLORER)
#define	PATH_MAX	(256)
#else
#include <limits.h>
#endif

extern	void OptionsAnalysis( int argc,  char * const *argv );
extern	void LogFile_LineArrange( char *buffer , int size );

typedef struct {
	float	*data;
	int		elem;
	char	*title;
} stGraphContent;

extern	void GraphPlot( int size, stGraphContent *data, char *file_name, int seq );

typedef enum {
	D_TERM_OF_INFINITY = 0,
	D_TERM_OF_LOG_END,
	D_TERM_OF_COUNT
} eTERM_EVENT;


struct st_ExecOptions {
	char			LogFile_9AXIS[PATH_MAX];
	char			LogFile_PPG[PATH_MAX];
	char			LogFile_COMMAND[PATH_MAX];
	char			LogFile_OUT[PATH_MAX];
	eTERM_EVENT		TermCondition;
	int				TermCount;
	int				TermFlag;
};

extern	struct st_ExecOptions	g_ExecOptions;

#endif	// defined(RUN_ON_PC)

#ifdef __cplusplus
}
#endif

#endif	// __FRIZZ_UTIL_H__
