#######
#
#  HUB files
#

##
#
##
SRCS	=
SRCS	+=	HUB/src/hub_main.c
SRCS	+=	HUB/src/init_table.c
SRCS	+=	HUB/src/test_data.c

##
#
##
INCS	=
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
INCS	+=	-I$(WORKSPACE_LOC)/$(STUBDIR)/include
endif
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz_hubhal/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libhub/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/activity
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/bikedetector
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/gesture
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/heartrate
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/libs
INCS	+=	-I$(WORKSPACE_LOC)/libalgorithm/libvsensor/inc/prs
INCS	+=	-I$(WORKSPACE_LOC)/libcommon/inc
INCS	+=	-I$(WORKSPACE_LOC)/target/libcommon/inc


##
#
##
ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
CFLAGS_LIB	+= -mrename-section-.text=.iram0.text
endif

##
#
##
LIBS	=	-L$(LIBDIR) -L$(WORKSPACE_LOC)/libalgorithm/libvsensor/bin/x86_obj/
LIBS	+=	-lsensor
LIBS	+=	-lalgorithm
LIBS	+=	-lND
ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
LIBS	+=	-lpdr
LIBS	+=	-lvital
endif
LIBS	+=	-lbase
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
LIBS	+=	-lxtensastab
endif
