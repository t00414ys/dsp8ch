/*!******************************************************************************
 * @file    stk3420.h
 * @brief   stk3420 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __STK3420_H__
#define __STK3420_H__
#include "stk3420_api.h"




/*
 *
 *		The rest of this is okay not change
 *
 */

#define STK3420_I2C_ADDRESS			0x58		///< Ambient Light Sensor, Proximity Sensor and 3D-Gesture Sensor (made from sensortek)


#define STK3420_REG_STATE			0x00
#define STK3420_REG_PS_GSCTRL1		0x01
#define STK3420_REG_ALSCTRL1		0x02
#define STK3420_REG_LEDCTRL			0x03
#define	STK3420_REG_INT				0x04
#define	STK3420_REG_WAIT1_PSGS		0x05
#define	STK3420_REG_THDH1_PS		0x06
#define	STK3420_REG_THDH2_PS		0x07
#define STK3420_REG_THDL1_PS		0x08
#define	STK3420_REG_THDL2_PS		0x09
#define	STK3420_REG_THDH1_ALS		0x0A
#define	STK3420_REG_TDHD2_ALS		0x0B
#define	STK3420_REG_THDL1_ALS		0x0C
#define STK3420_REG_THDL2_ALS		0x0D
#define	STK3420_REG_FLAG			0x10
#define	STK3420_REG_DATA1_PS		0x11
#define	STK3420_REG_DATA2_PS		0x12
#define	STK3420_REG_DATA1_ALS		0x13
#define	STK3420_REG_DATA2_ALS		0x14
#define	STK3420_REG_DATA1_IRS		0x17
#define	STK3420_REG_DATA2_IRS		0x18
#define	STK3420_REG_ALSCTRL2		0x19
#define	STK3420_REG_WAIT_ALS		0x1B
#define	STK3420_REG_WAIT2_PS		0x1C
#define	STK3420_REG_PS_GSCTRL2		0x1D
#define	STK3420_REG_GSFLAG			0x1E
#define	STK3420_REG_GSFIFOCTRL		0x1F
#define	STK3420_REG_DATA1_GSE		0x20
#define	STK3420_REG_DATA2_GSE		0x21
#define	STK3420_REG_DATA1_GSW		0x22
#define	STK3420_REG_DATA2_GSW		0x23
#define	STK3420_REG_DATA1_GSN		0x24
#define	STK3420_REG_DATA2_GSN		0x25
#define	STK3420_REG_DATA1_GSS		0x26
#define	STK3420_REG_DATA2_GSS		0x27
#define STK3420_REG_PRODUCT_ID		0x3E
#define	STK3420_REG_RESERVED		0x3F
#define	STK3420_REG_SOFT_RESET		0x80


#endif // __STK3420_H__

