/*!******************************************************************************
 * @file    mpuxxxx.h
 * @brief   mpuxxxx sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PAH8001_H__
#define __PAH8001_H__

#include "pah8001_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */
#define		MODE_100HZ		// If using PAW8000.a, please comment out. It means 20Hz mode



typedef unsigned char		uint8_t;
typedef unsigned short int	uint16_t;
typedef unsigned int		uint32_t;
typedef char				int8_t;
typedef short int			int16_t;
typedef int					int32_t;

#define ACCL_INTERVAL		40

#define PAH8001_I2C_ADDRESS					0x33
//#define PAH8001_I2C_ADDRESS					0x37
//#define PAH8001_I2C_ADDRESS					0x3b
//#define PAH8001_I2C_ADDRESS					0x53
//#define PAH8001_I2C_ADDRESS					0x57
//#define PAH8001_I2C_ADDRESS					0x5b
//#define PAH8001_I2C_ADDRESS					0x63
//#define PAH8001_I2C_ADDRESS					0x67
//#define PAH8001_I2C_ADDRESS					0x6b

#define PAH8001_WHOAMI_ID1					0x30
#define PAH8001_WHOAMI_ID2					0xD0


typedef enum {
	FLAG_DATA_READY = 0, 	//Bank1 0x68[3:0]=1;
	FLAG_DATA_NOT_READY, 	//Bank1 0x68[3:0]=0;
	FLAG_DATA_LOSS, 		//Bank1 0x68[3:0];
	FLAG_NO_TOUCH, 			//Bank0 0x59[7]=0;
	FLAG_DATA_ERROR,
	FLAG_POOR_SIGNAL, 		//Signal grade is under signal grade threshold=40;
	FLAG_FIFO_ERROR, 		//FIFO is not enough
	FLAG_TIMING_ERROR, 		//Polling timing error
} PXI_STATUS_FLAG;

#define LED_INC_DEC_STEP				(2)
#define LED_CTRL_EXPO_TIME_HI_BOUND		(496)
#define LED_CTRL_EXPO_TIME_LOW_BOUND	(32)
#define LED_CTRL_EXPO_TIME_HI			(420)
#define LED_CTRL_EXPO_TIME_LOW			(64)
#define LED_CURRENT_HI					(31)
#define LED_CURRENT_LOW					(1)

#if	defined(MODE_100HZ)
#define STATE_COUNT_TH				(6)
#else
#define STATE_COUNT_TH				(3)
#endif
#define DEFAULT_LED_STEP				(10)

//#define	D_USE_HYSTERESIS
//#define	LED_CTRL_HYSTERESIS_TIME		(2000)
#define	D_DATA_STANDARDIZATION
#if defined(D_DATA_STANDARDIZATION)
//#define	D_USE_NO_MOVE_LED_STEP
//#undef	DEFAULT_LED_STEP
//#define DEFAULT_LED_STEP					(31)
#define	D_DATA_STANDARDIZATION_OFFSET		(0x10)
#endif

//#define	D_USE_NEW_PIXART_ALGO_DRIVER

//#define	D_USE_AE_OFF
#if	defined(D_USE_AE_OFF)
#undef	DEFAULT_LED_STEP
#define DEFAULT_LED_STEP					(31)
#endif
#endif
