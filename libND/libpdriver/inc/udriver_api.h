/*!******************************************************************************
 * @file    udriver_api.h
 * @brief   udriver sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __UDRIVER_API_H__
#define __UDRIVER_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int init_xxx_xxx( unsigned int param );
void ctrl_xxx_xxx( int f_ena );
unsigned int rcv_xxx_xxx( unsigned int tick );
int conv_xxx_xxx( frizz_fp data[3] );

int xxx_setparam( void *ptr );
unsigned int xxx_get_ver( void );
unsigned int xxx_get_name( void );
int xxx_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif	// __UDRIVER_API_H__
