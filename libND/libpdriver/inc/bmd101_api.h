/*!******************************************************************************
 * @file    bmd101_api.h
 * @brief   bmd101 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMD101_API_H__
#define __BMD101_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int bmd101_init( unsigned int param );
void bmd101_ctrl( int f_ena );
unsigned int bmd101_rcv( unsigned int tick );
int bmd101_conv( frizz_fp data[3] );

int bmd101_setparam( void *ptr );
unsigned int bmd101_get_ver( void );
unsigned int bmd101_get_name( void );

int bmd101_get_condition( void *data );


#ifdef __cplusplus
}
#endif

#endif	// __BMD101_API_H__
