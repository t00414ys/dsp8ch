/*!******************************************************************************
 * @file    ppgemu_api.h
 * @brief   sample program for ppg sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PPG_EMULATION_H__
#define __PPG_EMULATION_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int  ppgemu_init( unsigned int param );
void ppgemu_ctrl( int f_ena );
unsigned int ppgemu_rcv( unsigned int tick );
int  ppgemu_conv( frizz_fp data[3] );

int ppgemu_setparam( void *ptr );
unsigned int ppgemu_get_ver( void );
unsigned int ppgemu_get_name( void );

int ppgemu_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif // __ZPA2326_H__

