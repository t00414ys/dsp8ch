/*!******************************************************************************
 * @file    zpa2326_api.h
 * @brief   zpa2326 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __STK3420_API_H__
#define __STK3420_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int stk3420_init( unsigned int param );

void stk3420_ctrl_prox( int f_ena );
void stk3420_ctrl_light( int f_ena );

unsigned int stk3420_rcv_prox( unsigned int tick );
int stk3420_conv_prox( frizz_fp *prox_data );

unsigned int stk3420_rcv_light( unsigned int tick );
int stk3420_conv_light( frizz_fp *light_data );

int stk3420_setparam( void *ptr );
unsigned int stk3420_get_ver( void );
unsigned int stk3420_get_name( void );

int stk3420_light_get_condition( void *data );
int stk3420_prox_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif // __STK3420_H__

