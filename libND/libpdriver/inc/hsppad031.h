/*!******************************************************************************
 * @file    hsppad031.h
 * @brief   hsppad031 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PRESS_ALPS_H__
#define __PRESS_ALPS_H__

#include "hsppad031_api.h"

/*
 *
 *		The rest of this is okay not change
 *
 */
#define PRES_ALPS_I2C_ADDRESS			0x48		///< Pressure Sensor(made from ALPS)
#define PRS_ALPS_ADD					0xAC		///< command

#endif
