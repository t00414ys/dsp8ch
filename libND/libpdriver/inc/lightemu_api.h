/*!******************************************************************************
 * @file    lightemu_api.h
 * @brief   sample program for light sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LIGHTEMU_API_H__
#define __LIGHTEMU_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

//
int  lightemu_init( unsigned int param );
void lightemu_ctrl( int f_ena );
unsigned int lightemu_rcv( unsigned int tick );
int  lightemu_conv( frizz_fp* data );

int lightemu_setparam( void *ptr );
unsigned int lightemu_get_ver( void );
unsigned int lightemu_get_name( void );

int lightemu_get_condition( void *data );

#ifdef __cplusplus
}
#endif


//
#endif
