/*!******************************************************************************
 * @file    pm131_api.h
 * @brief   pm131 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PM131_API_H__
#define __PM131_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif


int pm131_init( unsigned int param );
void pm131_ctrl( int f_ena );
unsigned int pm131_rcv( unsigned int tick );
int pm131_conv( frizz_fp data[3] );

int pm131_setparam( void *buffer );
unsigned int pm131_get_ver( void );
unsigned int pm131_get_name( void );

int pm131_calib_get_status( void *result );
int pm131_calib_get_data( void *data );
int pm131_calib_set_data( void *data );

int pm131_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
