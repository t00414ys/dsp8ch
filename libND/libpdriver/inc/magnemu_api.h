/*!******************************************************************************
 * @file    magnemu_api.h
 * @brief   sample program for magnet sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MANG_EMULATION__H__
#define __MANG_EMULATION__H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int  magnemu_init( unsigned int param );
void magnemu_ctrl( int f_ena );
unsigned int magnemu_rcv( unsigned int tick );
int  magnemu_conv( frizz_fp data[3] );

int magnemu_setparam( void *ptr );
unsigned int magnemu_get_ver( void );
unsigned int magnemu_get_name( void );

int magnemu_calib_get_status( void *result );
int magnemu_calib_get_data( void *data );
int magnemu_calib_set_data( void *data );


int magnemu_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif
