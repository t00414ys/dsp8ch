/*!******************************************************************************
 * @file    pm131.h
 * @brief   pm131 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PM131_H__
#define __PM131_H__
#include "pm131_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use magnet calibration
 *(DEFINITED) : Enable  the calibration process maker create
 *(~DEFINITED): Disable the calibration process maker create
 */
//#define	D_USE_CALIB_PM131



/*
 *
 *		The rest of this is okay not change
 *
 */

#define CTRL_ACTIVATE					(1)
#define CTRL_DEACTIVATE					(0)

#define PM131_MICRO_TESLA_PER_LSB		(1)	// Gauss-> uT

#define PM131_I2C_ADDRESS_C				(0x0C)		///< Magnet Sensor
#define PM131_I2C_ADDRESS_D				(0x0D)		///< Magnet Sensor
#define PM131_I2C_ADDRESS_E				(0x0E)		///< Magnet Sensor
#define PM131_I2C_ADDRESS_F				(0x0F)		///< Magnet Sensor

#define PM131_WHOAMI_ID			0xFF		// TBD ??
/*
 * REGISTER DEFINITION
*/
#define	PM131_WHOAMI			0x00
#define	PM131_MOREINFO			0x01
#define	PM131_STATUS1			0x02
#define	PM131_VALUE_XL			0x03
#define	PM131_VALUE_XH			0x04
#define	PM131_VALUE_YL			0x05
#define	PM131_VALUE_YH			0x06
#define	PM131_VALUE_ZL			0x07
#define	PM131_VALUE_ZH			0x08
#define	PM131_STATUS2			0x09
#define	PM131_CTRL1				0x0A
#define	PM131_CTRL2				0x0B
#define	PM131_TEMP_L			0x1C
#define	PM131_TEMP_H			0x1D

/*
 * VALUE DEFINITION
*/
#define	PM131_MODE_STANDBY		0
#define	PM131_MODE_1Hz			10
#define	PM131_MODE_10Hz			3
#define	PM131_MODE_20Hz			5
#define	PM131_MODE_50Hz			7
#define	PM131_MODE_100Hz		6
#define	PM131_MODE_SINGLE		1

#endif	// __PM131_H__
