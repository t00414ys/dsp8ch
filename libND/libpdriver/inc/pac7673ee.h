/*!******************************************************************************
 * @file    pac7673ee.h
 * @brief   pac7673ee sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PAC7673EE_H__
#define __PAC7673EE_H__
#include "pac7673ee_api.h"

/*
 *
 *		The rest of this is okay not change
 *
 */

#define PAC7673EE_I2C_ADDRESS			0x48		///< Ambient Light Sensor and Proximity Sensor(made from PixArt)

#define PAC7673EE_REG_CTRL			0x03
#define PAC7673EE_REG_ALS_CONFIG	0x04
#define PAC7673EE_REG_LED_PERIOD	0x05
#define PAC7673EE_REG_PS_CONFIG		0x06
#define PAC7673EE_REG_IDEL0			0x07
#define PAC7673EE_REG_IDEL1			0x08
#define PAC7673EE_REG_STAUS			0x09
#define PAC7673EE_REG_ALS_THD_H		0x0A
#define PAC7673EE_REG_ALS_THD_L		0x0B
#define PAC7673EE_REG_PS_THD_H		0x0C
#define PAC7673EE_REG_PS_THD_L		0x0D
#define PAC7673EE_REG_ALS_DATA0		0x0E
#define PAC7673EE_REG_ALS_DATA1		0x0F
#define PAC7673EE_REG_PS_DATA		0x10
#define PAC7673EE_REG_INT			0x11


#endif // __PAC7673EE_H__

