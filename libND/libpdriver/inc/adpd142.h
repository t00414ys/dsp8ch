/*!******************************************************************************
 * @file    adpd142.h
 * @brief   lt_1ph03 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef ADAP142_H
#define ADPD142_H
#include "adpd142_api.h"



/*
 *
 *		The rest of this is okay not change
 *
 */
#define ADPD142_I2C_ADDRESS				(0x64)
#define ADPD142_CHIP_ID					(0x16)


typedef	unsigned char		U8;
typedef	signed char			S8;
typedef	unsigned short		U16;
typedef	signed short		S16;
typedef	unsigned long		U32;
typedef	signed long			S32;



/* Exported types ---------------------------------------------------------- */

typedef enum {
	ADPDDrv_MODE_IDLE = 0,
	ADPDDrv_MODE_PAUSE,
	ADPDDrv_MODE_PWR_OFF,
	ADPDDrv_MODE_SAMPLE
} ADPDDrv_OPERATION_MODE;

typedef enum {
	ADPDDrv_SLOT_A = 0,
	ADPDDrv_SLOT_B,
	ADPDDrv_SLOT_AB
} ADPDDrv_OPERATION_SLOT;

/* Exported constants ------------------------------------------------------ */

/*  ADPD REGISTERS */
#define REG_INT_STATUS              0x00
#define REG_INT_MASK                0x01
#define REG_PAD_IO_CTRL             0x02
#define REG_I2CS_CTL_MATCH          0x06
#define REG_CHIP_ID                 0x08
#define REG_OP_MODE                 0x10
#define REG_OP_MODE_CFG             0x11
#define REG_SAMPLING_FREQ           0x12
#define REG_PD_SELECT               0x14
#define REG_DEC_MODE                0x15
#define REG_CH1_OFFSET_A            0x18
#define REG_CH2_OFFSET_A            0x19
#define REG_CH3_OFFSET_A            0x1A
#define REG_CH4_OFFSET_A            0x1B
#define REG_CH1_OFFSET_B            0x1E
#define REG_CH2_OFFSET_B            0x1F
#define REG_CH3_OFFSET_B            0x20
#define REG_CH4_OFFSET_B            0x21
#define REG_LED1_DRV                0x23
#define REG_LED2_DRV                0x24
#define REG_LED_TRIM                0x25
#define REG_PULSE_OFFSET_A          0x30
#define REG_PULSE_PERIOD_A          0x31
#define REG_PULSE_MASK              0x34
#define REG_PULSE_OFFSET_B          0x35
#define REG_PULSE_PERIOD_B          0x36
#define REG_AFE_CTRL_A              0x39
#define REG_AFE_CTRL_B              0x3B
#define REG_AFE_TRIM_A              0x42
#define REG_AFE_TEST_A              0x43
#define REG_AFE_TRIM_B              0x44
#define REG_AFE_TEST_B              0x45
#define REG_OSC32K                  0x4B
#define REG_TEST_PD                 0x52
#define REG_EFUSE_CTRL              0x57
#define REG_FIFO_CLK                0x5f
#define REG_DATA_BUFFER             0x60

#define REG_EFUSE_STATUS0           0x67

#define REG_X1_A                    0x64
#define REG_X2_A                    0x65
#define REG_Y1_A                    0x66
#define REG_Y2_A                    0x67
#define REG_X1_B                    0x68
#define REG_X2_B                    0x69
#define REG_Y1_B                    0x6A
#define REG_Y2_B                    0x6B
#define REG_READ_X1L_A              0x70
#define REG_READ_X2L_A              0x71
#define REG_READ_Y1L_A              0x72
#define REG_READ_Y2L_A              0x73
#define REG_READ_X1H_A              0x74
#define REG_READ_X2H_A              0x75
#define REG_READ_Y1H_A              0x76
#define REG_READ_Y2H_A              0x77
#define REG_READ_X1L_B              0x78
#define REG_READ_X2L_B              0x79
#define REG_READ_Y1L_B              0x7A
#define REG_READ_Y2L_B              0x7B
#define REG_READ_X1H_B              0x7C
#define REG_READ_X2H_B              0x7D
#define REG_READ_Y1H_B              0x7E
#define REG_READ_Y2H_B              0x7F

/*  REGISTER Values */
#define OP_IDLE_MODE            0
#define OP_PAUSE_MODE           1
#define OP_RUN_MODE             2

#define FIFO_CLK_EN             0x0001
#define FIFO_CLK_DIS            0x0000

// IRQ related
#define FIFO_CLR                0x8000
#define IRQ_CLR_ALL             0x00FF
#define IRQ_CLR_FIFO            0x8000
#define IRQ_MASK_RAW            0x0060
#define IRQ_RAW_AB_EN           0xC19F
#define IRQ_RAW_A_EN            0xC1DF
#define IRQ_RAW_B_EN            0xC1BF

// Slot related
#define SLOT_A_DATA_SIZE        0x8
#define SLOT_B_DATA_SIZE        0x8
#define SLOT_AB_DATA_SIZE       0x10
#define SLOT_MASK               0xF800
#define SLOT_A_MODE             0x0011
#define SLOT_B_MODE             0x0120
#define SLOT_AB_MODE            0x0131

#define ADPDDrv_SUCCESS (0)
#define ADPDDrv_ERROR (-1)

#define		D_SLOTA_CH1		(0)	// Green-1
#define		D_SLOTA_CH2		(1)	// Green-1
#define		D_SLOTA_CH3		(2)	// Green-2
#define		D_SLOTA_CH4		(3)	// Green-2
#define		D_SLOTB_CH1		(4)	// Red
#define		D_SLOTB_CH2		(5)	// Red
#define		D_SLOTB_CH3		(6)	// IR
#define		D_SLOTB_CH4		(7)	// IR

#endif // ADPD142_H
