/*!******************************************************************************
 * @file mes.h
 * @brief header for message register
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_COUNTER_H__
#define __FRIZZ_COUNTER_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	unsigned int	cur;
	unsigned int	max;
	unsigned int	min;
	unsigned int	cnt;
	unsigned int	ttl;
} FrizzCounter_h;

/**
 * @brief counter initialize
 *
 * @param h handle
 *
 * @note required to call function side secure object for malloc removal.
 */
void frizz_counter_init( FrizzCounter_h* h );

/**
 * @brief start measurement
 *
 * @param h handle
 */
void frizz_counter_start( FrizzCounter_h* h );

/**
 * @brief stop measurement
 *
 * @param h handle
 */
void frizz_counter_stop( FrizzCounter_h* h );

/**
 * @brief get calculation value last cycles
 *
 * @param h handle
 *
 * @return last cycles
 */
unsigned int frizz_counter_get_cur( FrizzCounter_h* h );

/**
 * @brief get calculation value  Max cycles
 *
 * @param h handle
 *
 * @return Max cycles
 */
unsigned int frizz_counter_get_max( FrizzCounter_h* h );

/**
 * @brief get calculation value Min cycles
 *
 * @param h handle
 *
 * @return Min cycles
 */
unsigned int frizz_counter_get_min( FrizzCounter_h* h );

/**
 * @brief calculation count
 *
 * @param h handle
 *
 * @return count value
 */
unsigned int frizz_counter_get_cnt( FrizzCounter_h* h );

/**
 * @brief get calculation value Total cycle
 *
 * @param h handle
 *
 * @return Total Cycle
 */
unsigned int frizz_counter_get_total( FrizzCounter_h* h );

#ifdef __cplusplus
}
#endif

#endif
