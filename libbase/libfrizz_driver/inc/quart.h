/*!******************************************************************************
 * @file quart.h
 * @brief Queue on UART header for DEBUG
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __QUART_H__
#define __QUART_H__
/** @addtogroup UART
 */
/** @ingroup UART */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif

/** @enum QUART_BAUDRATE_MODE
 *  @brief using buadrate preset
 */
typedef enum {
	QUART_BAUDRATE_115200 = 0,	///< 115,200 bps
	QUART_BAUDRATE_125000,		///< 125,000 bps
	QUART_BAUDRATE_1250000,		///< 1,250,000 bps
} QUART_BAUDRATE_MODE;

/**
 * @brief	Initialize QUART driver and UART driver with following setting:<BR>
 * 			Parity             : none<BR>
 * 			Stop bit           : 1 bit<BR>
 * 			Number of data bit : 8bit<BR>
 * 			Threshold of number of receive data in HW flow control is 121 byte.<BR>
 * 			UART interrupt is disabled.
 *
 * @param core_hz	frizz core clock frequency.<BR>
 *					When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *					When use internal OSC as frizz core clock, set 40MHz.
 * @param mode		Baurate to set<BR>
 *					set baudrate  with Enumeration QUART_BAUDRATE_MODE<BR>
 *					Though Baudrate can be also set directly, the rate may not be able to be the target rate due to division resolution.<BR>
 *					For the detail, please refer to "frizz Users Manual"<BR>
 * @param f_lock	set flow control.<BR>
 *					0:no flow control<BR>
 *					1:Hardware flow control<BR>
 * @return The return value is 0.
 */
int quart_init( unsigned int core_hz, QUART_BAUDRATE_MODE mode, int f_lock );

/**
 * @brief Convert UART input data to integer until receive data except for ASCII from '0' to 'F' (maximum 8 characters).
 *
 * @return Converted data
 */
int quart_in_pop( void );

/**
 * @brief Convert input integer to ASCII from '0' to 'F' and output to UART.
 *
 * @param	data	data to convert
 * @return	void
 */
void quart_out_push( int data );

/**
 * @brief	Output character string to UART (maximum 128 bytes).<BR>
 *			This API looks like "printf" of C language.
 *
 * @param	form	Output character string
 * @return	void
 */
void quart_out_raw( const char* form, ... );

/**
 * @brief	Wait until FIFO data received is up to 121 bytes.<BR>
 *			This function is blocked until then.<BR>s
 *			This function doesn't get data from UART receive FIFO.
 *
 * @return	void
 */
void quart_wait_full( void );

#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __QUART_H__
