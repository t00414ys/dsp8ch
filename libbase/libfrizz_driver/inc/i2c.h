/*!******************************************************************************
 * @file i2c.h
 * @brief header for I2C driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __I2C_H__
#define __I2C_H__
/** @addtogroup I2C
 */
/** @ingroup I2C */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif

//#define I2CLIB_DEBUG	// I2C LIB dubug mode

/**
 *  @struct sI2C_CMD
 *  @brief I2C Command Structure
 */
typedef struct {
	unsigned char	fread: 1;	///< 0:write, 1:read
	unsigned char	saddr: 7;	///< I2C Slave Address
	unsigned char	raddr;		///< (0x00~0xFE):Register Address (0xFF):Raw Mode(raddr ignore, fread type data transfered.)
	unsigned char	*data;		///< I/O Buffer
	unsigned int	size;		///< Transfer Size
} sI2C_CMD;

/** @enum eI2C_ERR
 *  @brief I2C Status of Error
 */
typedef enum {
	I2C_ERR_NONE = 0,	///< No error
	I2C_ERR_ACK,		///< NACK error
	I2C_ERR_ARBIT		///< Arbitration error
} eI2C_ERR;

#define	I2C_WRITE				(0)		///< I2C Write Command
#define	I2C_READ				(1)		///< I2C Read Command
#define	I2C_RAW_ACCESS			(0xFF)	///< I2C Raw Access Mode
#define	I2C_RESULT_SUCCESS		(0)		///< I2C Result Success

/**
 * @brief Callback when detected event
 *
 * @param [in] void* callback argument
 * @param [in] eI2C_ERR #eI2C_ERR Status of Error
 * @return void
 */
typedef void ( *i2c_callback )( void*, eI2C_ERR );

/**
 * @brief Enable I2C block and initialize I2C driver.
 *
 * @param [in]	core_freq	frizz core clock frequency<BR>
 *							When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *							When use internal OSC as frizz core clock, set 40MHz.
 * @param [in]	i2c_freq	Clock frequency using for I2C SCL.<BR>
 *							frizz supports Standard mode(Max.100kHz), Fast mode(Max.400kHz) and Fast mode+(Max.1MHz)
 * @retval	0	Success
 * @retval	-1	invalid parameter
 */
int i2c_init( const unsigned int core_freq, const unsigned int i2c_freq );

/**
 * @brief Disable I2C block and terminate I2C driver.
 *
 * @return void
 */
void i2c_end( void );

/**
 * @brief Synchronous transmits by I2C.<BR>
 *        This function is blocked until all data transmission described in the command is completed
 *
 * @param [in] cmd  I2C command to send
 * @param [out] err I2C transfer error code
 * @retval	0	Success
 * @retval	-1	Iniquitous parameter
 */
int i2c_transfer_sync( const sI2C_CMD *cmd, eI2C_ERR *err );

/**
 * @brief Asynchronous transmits by I2C.<BR>
 *        Even if this function is successful, I2C transmission isn't ensured.<BR>
 *        When the I2C transmission is completed, callback function is called.<BR>
 *        After this function is successful, executing "i2c_sync()" with sync_id can wait for completion of asynchronous transmits.
 *
 * @param [in]	cmd	I2C command to send
 * @param [in]	pf	Callback
 *					Pointer to callback function called when I2C transmission is completed.<BR>
 *					When calling callback function isn't necessary, set 0(NULL).
 * @param [in]	pd	Callback parameter
 * @retval	"More than 0"	sync_id (Success)
 * @retval	-1	Invalid parameter
 * @retval	-2	Command buffer full
 */
int i2c_transfer_add( const sI2C_CMD *cmd, i2c_callback pf, void *pd );

/**
 * @brief Get error status of the latest transmission.
 *
 * @return I2C transfer error code
 */
eI2C_ERR i2c_get_error( void );

/**
 * @brief Wait for I2C transfer completion of all "sync_id" is more than 0.<BR>
 *        This function is blocked until all data transmission is completed.
 *
 * @param [in]	sync_id	Sync_id(-1, 0-0x7FFFFFFF)<BR>
 *						Sync_id gotten from i2c_transfer_add().
 * @return	void
 *
 * @note (sync_id < 0): waiting for completion of all of transfer
 * @note No warrantee with invalid sync_id
 */
void i2c_sync( int sync_id );

/**
 * @brief Synchronous transmits by I2C.<BR>
 *        This function is blocked until all data transmission described in the command is completed
 *
 * @param [in] addr , host device address to send
 * @param [in] addr , host register address to send
 * @param [in] buff pointer, buffer to read
 * @param [in] length, how many bytes to read
 * @retval	0	Success
 * @retval	-1	Iniquitous parameter
 * @retval	1	NACK error
 * @retval	2	Arbitration error
  */
int i2c_read( unsigned char bus_addr, unsigned char data_addr, unsigned char *buff, char length );

/**
 * @brief Synchronous transmits by I2C.<BR>
 *        This function is blocked until all data transmission described in the command is completed
 *
 * @param [in] addr , host device address to send
 * @param [in] addr , host register address to send
 * @param [in] buff pointer, buffer to write
 * @param [in] length, how many bytes to write
 * @retval	0	Success
 * @retval	-1	Iniquitous parameter
 * @retval	1	NACK error
 * @retval	2	Arbitration error
 */
int i2c_write( unsigned char bus_addr, unsigned char data_addr, unsigned char *buff, char length );

#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __I2C_H__
