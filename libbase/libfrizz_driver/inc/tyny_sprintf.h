/*!******************************************************************************
 * @file    tyny_sprintf.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __TINY_SPRINTF_H__
#define __TINY_SPRINTF_H__

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif


extern void tiny_sprintf( char* buff, char* fmt, ... );
extern void tiny_printf( char* fmt, ... );
extern void tiny_vsprintf( char* buff, char* fmt, va_list arg );


#ifdef __cplusplus
}
#endif

#endif /* __TINY_SPRINTF_H__ */
