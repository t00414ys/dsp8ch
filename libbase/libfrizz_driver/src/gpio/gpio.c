/*!******************************************************************************
 * @file gpio.c
 * @brief Test Program for GPIO
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/hal.h>
#include "frizz_env.h"

#include "frizz_peri.h"
#include "gpio.h"

#define GPIOMD_OUTPUT	0
#define GPIOMD_INPUT	1
#define GPIOMD_PWM		2
#define GPIOMD_DISABLE	3
#define GPIOMD_HIGH_INT	2
#define GPIOMD_LOW_INT	3

/**
 * @brief GPIO Driver Context
 */
static
struct {
	void* args;
	gpio_callback fp;
} g_gpio;

static void GPIO_ISR( void )
{
	if( g_gpio.fp != 0 ) {
		g_gpio.fp( g_gpio.args );
	}
}

/**
 * @brief gpio initialize
 *
 * @param fp callback
 * @param args args for callback function
 */
void gpio_init( gpio_callback fp, void* args )
{
	unsigned int		host_gpio_trigger;
	g_gpio.fp = fp;
	g_gpio.args = args;
	if( frizz_use_mode.high_sleep_switch_source == 2 ) {
		host_gpio_trigger = INTERRUPT_NO_GPIO2;
	} else {
		host_gpio_trigger = INTERRUPT_NO_GPIO3;
	}
	_xtos_ints_off( 1 << host_gpio_trigger );
	_xtos_set_interrupt_handler( host_gpio_trigger, ( _xtos_handler )GPIO_ISR );
}

/**
 * @brief gpio mode setting
 *
 * @param gpio pin number
 * @param gpio output setting
 */
void gpio_set_mode( eGPIO_NO no, eGPIO_MODE mode )
{
	unsigned int bit;

	switch( mode ) {
	case GPIO_MODE_OUT:
		bit = GPIOMD_OUTPUT;
		switch( no ) {
		case GPIO_NO_0:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFFC );
			break;
		case GPIO_NO_1:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFF3 );
			break;
		case GPIO_NO_2:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFCF );
			break;
		case GPIO_NO_3:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFF3F );
			break;
		default:
			break;
		}
		break;
	case GPIO_MODE_IN:
		bit = GPIOMD_INPUT;
		switch( no ) {
		case GPIO_NO_0:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFFC );
			break;
		case GPIO_NO_1:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFF3 );
			break;
		case GPIO_NO_2:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFCF );
			break;
		case GPIO_NO_3:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFF3F );
			break;
		default:
			break;
		}
		break;
	case GPIO_MODE_PWM:
		bit = GPIOMD_PWM;
		switch( no ) {
		case GPIO_NO_0:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFFC ) | ( bit << 0 );
			bit = GPIOMD_OUTPUT;
			break;
		case GPIO_NO_1:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFF3 ) | ( bit << 2 );
			bit = GPIOMD_OUTPUT;
			break;
		default:
			break;
		}
		break;
	case GPIO_MODE_IN_PINT:
		bit = ( ( no == GPIO_NO_2 ) || ( no == GPIO_NO_3 ) ) ? GPIOMD_HIGH_INT : GPIOMD_DISABLE;
		switch( no ) {
		case GPIO_NO_2:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFCF );
			break;
		case GPIO_NO_3:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFF3F );
			break;
		default:
			break;
		}
		break;
	case GPIO_MODE_IN_NINT:
		bit = ( ( no == GPIO_NO_2 ) || ( no == GPIO_NO_3 ) ) ? GPIOMD_LOW_INT : GPIOMD_DISABLE;
		switch( no ) {
		case GPIO_NO_2:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFFCF );
			break;
		case GPIO_NO_3:
			*REGALT_FUNC = ( *REGALT_FUNC & 0xFF3F );
			break;
		default:
			break;
		}
		break;
	default:
		bit = GPIOMD_DISABLE;
		break;
	}
	switch( no ) {
	case GPIO_NO_0:
		*REGGPIO_MD = ( *REGGPIO_MD & 0xFFFE ) | ( bit << 0 );
		break;
	case GPIO_NO_1:
		*REGGPIO_MD = ( *REGGPIO_MD & 0xFFFD ) | ( bit << 1 );
		break;
	case GPIO_NO_2:
		*REGGPIO_MD = ( *REGGPIO_MD & 0xFFF3 ) | ( bit << 2 );
		break;
	case GPIO_NO_3:
		*REGGPIO_MD = ( *REGGPIO_MD & 0xFFCF ) | ( bit << 4 );
		break;
	default:
		break;
	}
}

/**
 * @brief gpio interrupt setting
 *
 * @param f_int
 */
void gpio_set_interrupt( int f_int )
{
	unsigned int	host_gpio_trigger;

	if( frizz_use_mode.high_sleep_switch_source == 2 ) {
		host_gpio_trigger = INTERRUPT_NO_GPIO2;
	} else {
		host_gpio_trigger = INTERRUPT_NO_GPIO3;
	}

	if( f_int ) {
		_xtos_ints_on( 1 << host_gpio_trigger );
	} else {
		_xtos_ints_off( 1 << host_gpio_trigger );
	}
}

/**
 * @brief gpio toggle output value setting
 *
 * @param no
 */
void gpio_toggle_data( eGPIO_NO no )
{
	unsigned int st = *REGGPIO_ST;
	st >>= 8;
	switch( no ) {
	case GPIO_NO_0:
		*REGGPIO_ST = ( st ) ^ ( 1 << 0 );
		break;
	case GPIO_NO_1:
		*REGGPIO_ST = ( st ) ^ ( 1 << 1 );
		break;
	case GPIO_NO_2:
		*REGGPIO_ST = ( st ) ^ ( 1 << 2 );
		break;
	case GPIO_NO_3:
		*REGGPIO_ST = ( st ) ^ ( 1 << 3 );
		break;
	default:
		break;
	}
}

/**
 * @brief gpio output value setting
 *
 * @param no
 * @param bit
 */
void gpio_set_data( eGPIO_NO no, int bit )
{
	unsigned int st = *REGGPIO_ST;
	st >>= 8;
	bit = ( bit ) ? 1 : 0;
	switch( no ) {
	case GPIO_NO_0:
		*REGGPIO_ST = ( st & 0xe ) | ( bit << 0 );
		break;
	case GPIO_NO_1:
		*REGGPIO_ST = ( st & 0xd ) | ( bit << 1 );
		break;
	case GPIO_NO_2:
		*REGGPIO_ST = ( st & 0xb ) | ( bit << 2 );
		break;
	case GPIO_NO_3:
		*REGGPIO_ST = ( st & 0x7 ) | ( bit << 3 );
		break;
	default:
		break;
	}
}

/**
 * @brief gpio data getting
 *
 * @param no
 *
 * @return bit
 */
int gpio_get_data( eGPIO_NO no )
{
	int bit = -1;
	switch( no ) {
	case GPIO_NO_0:
		bit = ( *REGGPIO_ST >> 0 ) & 0x1;
		break;
	case GPIO_NO_1:
		bit = ( *REGGPIO_ST >> 1 ) & 0x1;
		break;
	case GPIO_NO_2:
		bit = ( *REGGPIO_ST >> 2 ) & 0x1;
		break;
	case GPIO_NO_3:
		bit = ( *REGGPIO_ST >> 3 ) & 0x1;
		break;
	default:
		break;
	}
	return bit;
}

/**
 * @brief setting frequency of PWM output
 *
 * @param core_freq frequency of system clock
 * @param pwm_freq output frequency of PWM
 *
 * @note please correctly set GPIO mode by #gpio_set_mode(), before call this function.
 */
void gpio_set_pwm0_freq( int core_freq, int pwm_freq )
{
	int target;
	target = ( pwm_freq == 0 ) ? 0 : core_freq / ( 16 * pwm_freq );
	*REGPWM0_C0 = target;
}

/**
 * @brief PWM output frequency cycle width setting
 *
 * @param pulse_count [7:0]
 */
void gpio_set_pwm0_pulse_count( int pulse_count )
{
	if( pulse_count > 0xFF ) {
		pulse_count = 0xFF;
	}

	*REGPWM0_C1 = pulse_count;
}

/**
 * @brief High pulse width setting
 *
 * @param high
 */
void gpio_set_pwm0_duty_count( int high )
{
	int pulse_count = *REGPWM0_C1;
	if( high < pulse_count ) {
		high = pulse_count - 1;
	}

	*REGPWM0_D0 = high;
}

/**
 * @brief setting frequency of PWM output
 *
 * @param core_freq frequency of system clock
 * @param pwm_freq output frequency of PWM
 *
 * @note please correctly set GPIO mode by #gpio_set_mode(), before call this function.
 */
void gpio_set_pwm1_freq( int core_freq, int pwm_freq )
{
	int target;
	target = ( pwm_freq == 0 ) ? 0 : core_freq / ( 16 * pwm_freq );
	*REGPWM1_C0 = target;
}

/**
 * @brief PWM output frequency cycle width setting
 *
 * @param pulse_count [7:0]
 */
void gpio_set_pwm1_pulse_count( int pulse_count )
{
	if( pulse_count > 0xFF ) {
		pulse_count = 0xFF;
	}

	*REGPWM1_C1 = pulse_count;
}

/**
 * @brief High pulse width setting
 *
 * @param high
 */
void gpio_set_pwm1_duty_count( int high )
{
	int pulse_count = *REGPWM1_C1;
	if( high < pulse_count ) {
		high = pulse_count - 1;
	}

	*REGPWM1_D0 = high;
}
