/*!******************************************************************************
 * @file timer.c
 * @brief source code for timer driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "timer.h"
#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_util.h"

#include <xtensa/config/core.h>
#include <xtensa/xtruntime.h>
#include <xtensa/hal.h>

#define	D_MAX_TIMERS		3			// Timer0:IntLevel-1,Timer1:IntLevel-2,Timer2:IntLevel-3
EXTERN_C void check_frizz_times( unsigned long *sec_cnt, unsigned long *nsec_cnt );


typedef struct {
	int no;
	int int_no;
	timer_callback pf;
	void* arg;
	volatile int status;	// 0:idle, 1:counting
	unsigned int cycles;
	unsigned int count;
	volatile unsigned int f_trig;
} s_TimerInfo;
s_TimerInfo g_timer_info[D_MAX_TIMERS];

void timer_handler( s_TimerInfo* info )
{
	if( info->status == 1 ) {
		unsigned int prev_val = xthal_get_ccompare( info->no );
		unsigned int diff = xthal_get_ccount() - prev_val;
		do {
			// count
			info->count++;
			// update
			unsigned next_val = prev_val + info->cycles;
			xthal_set_ccompare( info->no, next_val );
			diff = xthal_get_ccount() - prev_val;
			prev_val = next_val;
		} while( info->cycles < diff );
		info->f_trig = 1;
		if( info->pf != 0 ) {
			info->pf( info->arg, info->count );
		}
	}
}

int timer_init( int no, timer_callback pf, void* arg )
{
	if( D_MAX_TIMERS <= no ) {
		return -1;
	}
	g_timer_info[no].no = no;
	g_timer_info[no].status = 0;	// idle
	g_timer_info[no].pf = pf;
	g_timer_info[no].arg = arg;
	g_timer_info[no].count = 0;
	g_timer_info[no].f_trig = 0;
	switch( no ) {
	case 1:
		g_timer_info[no].int_no = INTERRUPT_NO_TIMER1;
		break;
	case 2:
		g_timer_info[no].int_no = INTERRUPT_NO_TIMER2;
		break;
	case 0:
	default:
		g_timer_info[no].int_no = INTERRUPT_NO_TIMER0;
		break;
	}
	timer_stop( no );
	_xtos_set_interrupt_handler_arg( g_timer_info[no].int_no, ( _xtos_handler )timer_handler, &g_timer_info[no] );
	return 0;
}

int timer_start( int no, unsigned int core_freq, unsigned int msec )
{
	if( D_MAX_TIMERS <= no ) {
		return -1;
	}
	if( g_timer_info[no].status == 0 ) {
		g_timer_info[no].cycles = ( ( core_freq + 500 ) / 1000 ) * msec;
		g_timer_info[no].count = 0;
		g_timer_info[no].status = 1;	// counting
		xthal_set_ccompare( g_timer_info[no].no, xthal_get_ccount() + g_timer_info[no].cycles );
		_xtos_ints_on( 1 << g_timer_info[no].int_no );
	}
	return 0;
}

//20171024�ǉ�
int timer_start_usec( int no, unsigned int core_freq, unsigned int usec )
{
	if( D_MAX_TIMERS <= no ) {
		return -1;
	}
	if( g_timer_info[no].status == 0 ) {
		//g_timer_info[no].cycles = ( ( core_freq + 500 ) / 1000 ) * msec;
		g_timer_info[no].cycles = ( ( ( ( core_freq + 500 ) / 1000 ) + 500 ) / 1000 ) * usec;
		g_timer_info[no].count = 0;
		g_timer_info[no].status = 1;	// counting
		xthal_set_ccompare( g_timer_info[no].no, xthal_get_ccount() + g_timer_info[no].cycles );
		_xtos_ints_on( 1 << g_timer_info[no].int_no );
	}
	return 0;
}

int timer_stop( int no )
{
	if( D_MAX_TIMERS <= no ) {
		return -1;
	}
	_xtos_ints_off( 1 << g_timer_info[no].int_no );
	g_timer_info[no].status = 0;	// idle
	return 0;
}

unsigned int timer_wait( int no )
{
	if( D_MAX_TIMERS <= no ) {
		return ( unsigned int ) - 1;
	}
	if( g_timer_info[no].status == 1 ) {
		while( g_timer_info[no].f_trig == 0 ) {
		}
		g_timer_info[no].f_trig = 0;
	}
	return g_timer_info[no].count;
}

unsigned int get_timer_ts( void )
{
	return g_timer_info[TIMER_ID_UNUSE_BLOCKING].count;
}


/**
 * @brief	Wait for timer interrupt with busy loop.<BR>
 *			This function is used Timer2<BR>
 *			This function is blocked until timer interrupt occurs.<BR>
 *			After timer interruption, timer interrupt count is returned.<BR>
 *			This interrupt count is cleared at timer starts or stops, and is incremented when timer interrupt occurred.
 *
 * @return void
*/
void mdelay( unsigned int msec )
{
	if( msec != 0 ) {
		timer_init( TIMER_ID_DELAY, ( timer_callback )0, 0 );
		timer_start( TIMER_ID_DELAY, g_ROSC2_FREQ, msec );
		timer_wait( TIMER_ID_DELAY );
	}
	return;
}

//20171024�ǉ�
void udelay( unsigned int usec )
{
	if( usec != 0 ) {
		timer_init( TIMER_ID_DELAY, ( timer_callback )0, 0 );
		timer_start_usec( TIMER_ID_DELAY, g_ROSC2_FREQ, usec );
		timer_wait( TIMER_ID_DELAY );
	}
	return;
}



/**
 * @brief get frizz time
 *
 * @param [in] sec 	sec pointer
 * @param [in] nsec nano sec pointer
 *
 * @return none
 */
void get_frizz_times( frizz_timeval *tv )
{

	_xtos_set_intlevel( INTERRUPT_LEVEL_AWAKE );	// enable interrupt level 4
	if( tv != 0 ) {
		check_frizz_times( &tv->sec, &tv->nsec );
	} else {
		check_frizz_times( 0, 0 );
	}
	_xtos_set_intlevel( 0 );
	return;
}

/**
 * @brief Measure the time
 *
 * @param [in] start clock count
 *
 * @return This function returns if A is 0 B.
 *         Unless B is zero, it returns in the ns time between A and B.
 */
unsigned long frizz_time_measure( unsigned long start )
{
	unsigned long			clk = 0;

#if !defined(RUN_ON_PC)
	__asm__( "rsr.ccount %0" : "=a"( clk ) );
#endif
	if( start != 0 ) {
		clk = clk - start;
		clk = clk * ( 1000000000UL / g_ROSC2_FREQ );		// Clock to nano sec
	}

	return clk;
}


/**
 * @brief Convert ns time to timeval
 *
 * @param [in] src  Pointer to put the results
 * @param [in] nsec source data
 *
 * @return This function returns if A is 0 B.
 *         Unless B is zero, it returns in the ns time between A and B.
 */
void frizz_time_to_timeval( frizz_timeval *src, unsigned long nsec )
{
	if( src != 0 ) {
		src->sec  = nsec / 1000000000UL;
		src->nsec = nsec % 1000000000UL;
	}
	return;
}

/**
 * @brief add frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return none
 *
 * @note src = src + dst
 */
void frizz_timevaladd( frizz_timeval *src, frizz_timeval *dst )
{
	if( ( src != 0 ) && ( dst != 0 ) ) {
		src->sec  = src->sec  + dst->sec;
		src->nsec = src->nsec + dst->nsec;
		if( src->nsec >= 1000000000UL ) {
			src->nsec = src->nsec - 1000000000UL;
			src->sec++;
		}
	}
	return;
}

/**
 * @brief del frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return none
 *
 * @note src = src - dst
 */
void frizz_timevaldel( frizz_timeval *src, frizz_timeval *dst )
{
	if( ( src != 0 ) && ( dst != 0 ) ) {
		src->sec  = src->sec - dst->sec;
		if( src->nsec < dst->nsec ) {
			src->sec--;
			src->nsec = src->nsec + 1000000000UL;
		}
		src->nsec = src->nsec - dst->nsec;
	}
	return;
}


/**
 * @brief del frizz time
 *
 * @param [in] src source time data
 * @param [in] div a dividend.
 *
 * @return none
 *
 * @note src = src / div
 */
void frizz_timevaldiv( frizz_timeval *src, unsigned int div )
{
	unsigned long long 	rest;
	if( ( src != 0 ) && ( div != 0 ) ) {
		rest = src->sec;
		rest = rest % div;
		src->sec  = src->sec / div;
		//
		rest = rest * 1000000000UL;
		rest = rest + src->nsec;

		rest = rest  / div;

		src->nsec = rest % 1000000000UL;
		src->sec  = src->sec + ( rest / 1000000000UL );
	}
	return;
}

/**
 * @brief compare frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return if(src > dst) return 1; else if(src < dst) resturn -1; else return 0;
 */
int frizz_timevalcmp( frizz_timeval *src, frizz_timeval *dst )
{
	if( ( src != 0 ) && ( dst != 0 ) ) {
		if( src->sec > dst->sec ) {
			return 1;
		} else if( src->sec < dst->sec ) {
			return -1;
		} else {
			if( src->nsec > dst->nsec ) {
				return 1;
			} else if( src->nsec < dst->nsec ) {
				return -1;
			}
		}
	}
	return 0;
}


