/*!******************************************************************************
 * @file spi.c
 * @brief header for SPI master driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_peri.h"
#include "spi.h"

/**
 * @name CTRL0
 * @{ */
#define SPI_CTRL0_KEEP_CS	(1 << 1)	///< Byte transfer MCSBx keep
#define SPI_CTRL0_TRANS		(1 << 0)	///< transfer start
/**  @} */

/**
 * @name CTRL1
 * @{ */
#define SPI_CTRL1_SPI_MODE0	(2 << 0)	///< SPI mode0
#define SPI_CTRL1_SPI_MODE2	(3 << 0)	///< SPI mode2
#define SPI_CTRL1_MCS0_EN	(1 << 4)	///< spi clock output enable + mcs0 enable
#define SPI_CTRL1_MCS1_EN	(1 << 5)	///< spi clock output enable + mcs1 enable
#define SPI_CTRL1_MCS2_EN	(1 << 6)	///< spi clock output enable + mcs2 enable
#define SPI_CTRL1_MCS3_EN	(1 << 7)	///< spi clock output enable + mcs3 enable

#define SPI_CTRL1_DATA_SEND_INT_EN	(1 << 2)	///< data send completion interrupt enable
#define SPI_CTRL1_IDLE_HI	(1 << 0)	///< MCLK initial value High
/**  @} */

//#define	USE_SPI_INTERRUPT	///< interrupt use flag (default not use)

#if	defined(USE_SPI_INTERRUPT)
static void int_handler( void )
{
	*REGSPI_INTSPIM |= ( volatile unsigned short )0x01;
	return;
}
#endif

/**
 *  @brief SPI chip select configure
 *
 *  @param [in] cs_no		set chip select ctrl1/altfunc register
 */
static void spi_set_chip_select( unsigned int cs_no )
{
	// select MCSB
	if( 3 < cs_no ) {
		cs_no = 3;
	}

	switch( cs_no ) {
	case 0:
		*REGALT_FUNC = ( *REGALT_FUNC & 0xFFFC ) | 0x0001;
		*REGSPI_CTRL1 |= SPI_CTRL1_MCS0_EN;
		break;
	case 1:
		*REGALT_FUNC = ( *REGALT_FUNC & 0xFFF3 ) | 0x0004;
		*REGSPI_CTRL1 |= SPI_CTRL1_MCS1_EN;
		break;
	case 2:
		*REGALT_FUNC = ( *REGALT_FUNC & 0xFFCF ) | 0x0010;
		*REGSPI_CTRL1 |= SPI_CTRL1_MCS2_EN;
		break;
	case 3:
		*REGALT_FUNC = ( *REGALT_FUNC & 0xFF3F ) | 0x0040;
		*REGSPI_CTRL1 |= SPI_CTRL1_MCS3_EN;
		break;
	default:
		break;
	}
	return;
}

/**
 * @brief SPI initialize
 *
 * @param [in]	core_freq	core clock
 * @param [in]	target_freq	spi clock
 * @param [in]	mode 		number of SPI mode (0or2)
 * @param [in]	cs_no		chip select number(0to3)
 */
void spi_init( unsigned int core_freq, unsigned int target_freq, unsigned char mode, unsigned int in_cs_no )
{
	unsigned int cycles;
	volatile unsigned short clkgenh, clkgenl, ctrl1 = 0;

	if( ( core_freq >> 1 ) < target_freq ) {
		target_freq = core_freq >> 1;
	}
	cycles = ( core_freq / target_freq ) - 2;

	if( mode == 0 ) {
		clkgenl = cycles >> 1;
		clkgenh = cycles - clkgenl;
		ctrl1 = SPI_CTRL1_SPI_MODE0;
	} else if( mode == 2 ) {
		clkgenh = cycles >> 1;
		clkgenl = cycles - clkgenh;
		ctrl1 = SPI_CTRL1_SPI_MODE2;
	} else { // mode 0
		clkgenl = cycles >> 1;
		clkgenh = cycles - clkgenl;
		ctrl1 = SPI_CTRL1_SPI_MODE0;
	}

	*REGSPI_CLKGENH = clkgenh;
	*REGSPI_CLKGENL = clkgenl;
	*REGSPI_CTRL1 = ctrl1;

#if	defined(USE_SPI_INTERRUPT)
	*REGSPI_CTRL1 |= SPI_CTRL1_DATA_SEND_INT_EN;

	_xtos_ints_off( 1 << INTERRUPT_NO_SPI );
	// setting ISR
	_xtos_set_interrupt_handler( INTERRUPT_NO_SPI, ( _xtos_handler )int_handler );
	// setting IMask
	_xtos_ints_on( 1 << INTERRUPT_NO_SPI );
#endif

	spi_set_chip_select( in_cs_no );

	return;
}

/**
 * @brief SPI data send/receive
 *
 * @param [in] tx_data		transfer data buffer
 * @param [out] rx_data		receive data buffer
 * @param [in] size			transfer data size
 * @param [in] cs_num		select chip select number(0~3)
 * @param [in] f_cs_keep	cs keep flag
 */
void spi_trans_data( unsigned char* tx_data, unsigned char* rx_data, unsigned int size, unsigned int cs_num, int f_cs_keep )
{
	volatile unsigned short ctrl0 = 0;
	unsigned int count;

	if( cs_num > 3 ) {
		cs_num = 3;
	}
	// setting
	ctrl0 =	( cs_num << 2 ) | SPI_CTRL0_KEEP_CS | SPI_CTRL0_TRANS;

	// transfer
	for( count = 0; count < size; count++ ) {
		// Tx
		if( tx_data != 0 ) {
			*REGSPI_TXREG = *tx_data;
			tx_data++;
		} else {
			*REGSPI_TXREG = 0;
		}
		if( ( f_cs_keep == 0 ) && ( ( count + 1 ) == size ) ) {
			ctrl0 &= ~SPI_CTRL0_KEEP_CS;
		}
		// start transfer
		*REGSPI_CTRL0 = ctrl0;

#if	defined(USE_SPI_INTERRUPT)
		_xtos_set_intlevel( INTERRUPT_LEVEL_SPI );
		while( *REGSPI_CTRL0 & SPI_CTRL0_TRANS ) {
			XT_WAITI( 0 );
			_xtos_set_intlevel( INTERRUPT_LEVEL_SPI );
		}
		_xtos_set_intlevel( 0 );
#else
		// wait complete
		while( *REGSPI_CTRL0 & SPI_CTRL0_TRANS );
#endif
		// Rx
		if( rx_data != 0 ) {
			*rx_data = *REGSPI_RXREG;
			rx_data++;
		}
	}
	return;
}

