/*!******************************************************************************
 * @file i2c.c
 * @brief Test Program for i2c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/tie/xt_interrupt.h>

#include "i2c.h"
#include "frizz_peri.h"

// I2C Direction in SlaveAddress
#define I2CWR 0
#define I2CRD 1

// I2c_CTR
#define I2C_CTR_CORE_ENABLE		0x80
#define I2C_CTR_INT_ENABLE		0x40

// I2C_CR
#define I2C_CR_START	0x80
#define I2C_CR_STOP		0x40
#define I2C_CR_READ		0x20
#define I2C_CR_WRITE	0x10
#define I2C_CR_NACK		0x08
#define I2C_CR_CLRINT	0x01

// I2C_SR
#define I2C_SR_NACK			0x80
#define I2C_SR_START		0x40
#define I2C_E_ARBIT			0x20
#define I2C_SR_TX			0x02
#define I2C_SR_INTERRUPT	0x01

// Internal Parameter
#define I2C_CMD_BUFF_NUM	32
#define I2C_ID_MAX		0x7FFF
#define I2C_ID_HALF		((short)(((unsigned int)I2C_ID_MAX+1)>>1))

// I2C Driver Status
typedef enum {
	I2C_STATUS_IDLE = 0,
	I2C_STATUS_W_SADDR,
	I2C_STATUS_R_SADDR,
	I2C_STATUS_RADDR,
	I2C_STATUS_DATA,
	I2C_STATUS_COMP,
	I2C_STATUS_ERR,
	I2C_STATUS_ERR_STOP,
} eI2C_STATUS;

typedef struct {
	int				sync_id;
	i2c_callback	pf;
	void			*pd;
	sI2C_CMD		cmd;
} sI2C_CMDBuf;

// Command Buffer
typedef struct {
	sI2C_CMDBuf		buf[I2C_CMD_BUFF_NUM];
	unsigned short	max;
	unsigned short	idx;
	unsigned short	num;
} sI2C_CMDList;

// I2C Driver Context
typedef struct {
	volatile eI2C_STATUS		status;		///< Driver Status
	eI2C_ERR					err;		///< Error Status
	sI2C_CMDList				list;		///< command buffer
	short						seed;		///< seed for ID
	volatile short				cur_id;		///< current sync_id
} s_I2CInfo;
s_I2CInfo g_i2c;	// object

/************************************
 * Inner Function
 ************************************/
static eI2C_ERR check_error( unsigned char sr, unsigned int fread )
{
	eI2C_ERR err = I2C_ERR_NONE;

	if( sr & I2C_E_ARBIT ) {
		// arbitration lost
		err = I2C_ERR_ARBIT;
	}
	// check ACK
	if( err == I2C_ERR_NONE ) {
		if(
			( g_i2c.status == I2C_STATUS_W_SADDR ) ||
			( g_i2c.status == I2C_STATUS_R_SADDR ) ||
			( g_i2c.status == I2C_STATUS_RADDR ) ||
			( ( g_i2c.status == I2C_STATUS_DATA ) && ( !fread ) )
		) {
			if( sr & I2C_SR_NACK ) {
				err = I2C_ERR_ACK;
			}
		}
	}
	return err;
}

static void snd_saddr( sI2C_CMD *cmd, unsigned char fread )
{
	unsigned char saddr = ( cmd->saddr << 1 );
	if( fread ) {
		saddr |= 1;
	}
	*REGI2C_TXR = saddr;
	*REGI2C_CR = I2C_CR_WRITE | I2C_CR_START;
}

static void snd_raddr( sI2C_CMD *cmd )
{
	*REGI2C_TXR = cmd->raddr;
	*REGI2C_CR = I2C_CR_WRITE;
}

static int snd_data( sI2C_CMD *cmd )
{
	unsigned char cr = 0;
	if( 0 < cmd->size ) {
		*REGI2C_TXR = *( cmd->data );
		cmd->data++;
		cmd->size--;
		// make command
		cr |= I2C_CR_WRITE;
		if( cmd->size == 0 ) {
			// last one
			cr |= I2C_CR_STOP;
		}
		*REGI2C_CR = cr;
	}
	return ( int ) cmd->size;
}

static int rcv_data_pre( sI2C_CMD *cmd )
{
	unsigned char cr = 0;
	if( 0 < cmd->size ) {
		// make command
		cr |= I2C_CR_READ;
		if( cmd->size == 1 ) {
			// last one
			cr |= ( I2C_CR_NACK | I2C_CR_STOP );
		}
		*REGI2C_CR = cr;
	}
	return ( int ) cmd->size;
}

static int rcv_data_post( sI2C_CMD *cmd )
{
	if( 0 < cmd->size ) {
		*( cmd->data ) = *REGI2C_RXR;
		cmd->data++;
		cmd->size--;
	}
	return ( int ) cmd->size;
}

static void kick( void )
{
	if( g_i2c.status == I2C_STATUS_IDLE ) {
		if( 0 < g_i2c.list.num ) {
			if( g_i2c.list.buf[g_i2c.list.idx].cmd.raddr != 0xFF ) {
				snd_saddr( &( g_i2c.list.buf[g_i2c.list.idx].cmd ), I2CWR );
				g_i2c.status = I2C_STATUS_W_SADDR;
			} else {
				// Raw mode
				if( g_i2c.list.buf[g_i2c.list.idx].cmd.fread ) {
					snd_saddr( &( g_i2c.list.buf[g_i2c.list.idx].cmd ), I2CRD );
				} else {
					snd_saddr( &( g_i2c.list.buf[g_i2c.list.idx].cmd ), I2CWR );
				}
				if( g_i2c.list.buf[g_i2c.list.idx].cmd.size == 0 ) {
					g_i2c.status = I2C_STATUS_COMP;
				} else {
					g_i2c.status = I2C_STATUS_W_SADDR;
				}
			}
		}
	}
}

static void sync( int sync_id )
{
	// interrupt block
	_xtos_set_intlevel( INTERRUPT_LEVEL_I2C );
	if( sync_id < 0 ) {
		// point to last id
		if( g_i2c.seed == 0 ) {
			sync_id = I2C_ID_MAX;
		} else {
			sync_id = g_i2c.seed - 1;
		}
	}
	while( g_i2c.status != I2C_STATUS_IDLE ) {
		volatile int diff_id = g_i2c.cur_id - sync_id;
		if( diff_id < -I2C_ID_HALF ) {
			diff_id += I2C_ID_MAX;
			diff_id++;
		}
		if( 0 < diff_id ) {
			break;
		}
		// status is running, block mode remove and sleep.
		XT_WAITI( 0 );
		// re'block
		_xtos_set_intlevel( INTERRUPT_LEVEL_I2C );
	}
	// block mode checked success, block mode remove.
	_xtos_set_intlevel( 0 );
}

static void int_handler( void )
{
	sI2C_CMDList *list = &( g_i2c.list );
	sI2C_CMDBuf *buf = &( list->buf[list->idx] );
	sI2C_CMD	*cmd = &( buf->cmd );
	// clear Interrupt
	*REGI2C_CR = I2C_CR_CLRINT;
	// check error
	if( g_i2c.status == I2C_STATUS_IDLE ) {
		// fail safe
		g_i2c.status = I2C_STATUS_IDLE;
		return;
	}
	if( g_i2c.status != I2C_STATUS_ERR ) {
		g_i2c.err = check_error( *REGI2C_SR, cmd->fread );
		if( g_i2c.err != I2C_ERR_NONE ) {
			g_i2c.status = I2C_STATUS_ERR_STOP;
		}
	}
	// execute
	switch( g_i2c.status ) {
	case I2C_STATUS_W_SADDR:
		if( cmd->raddr != 0xFF ) {
			snd_raddr( cmd );
			g_i2c.status = I2C_STATUS_RADDR;
			if( cmd->size == 0 ) {
				g_i2c.status = I2C_STATUS_COMP;
			}
		} else {
			if( cmd->fread ) {
				if( rcv_data_pre( cmd ) == 1 ) {
					g_i2c.status = I2C_STATUS_COMP;
				} else {
					g_i2c.status = I2C_STATUS_DATA;
				}
			} else {
				if( snd_data( cmd ) == 0 ) {
					g_i2c.status = I2C_STATUS_COMP;
				} else {
					g_i2c.status = I2C_STATUS_DATA;
				}
			}
		}
		break;
	case I2C_STATUS_R_SADDR:
		if( rcv_data_pre( cmd ) == 1 ) {
			g_i2c.status = I2C_STATUS_COMP;
		} else {
			g_i2c.status = I2C_STATUS_DATA;
		}
		break;
	case I2C_STATUS_RADDR:
		if( cmd->fread ) {
			snd_saddr( cmd, I2CRD );
			g_i2c.status = I2C_STATUS_R_SADDR;
		} else {
			if( snd_data( cmd ) == 0 ) {
				g_i2c.status = I2C_STATUS_COMP;
			} else {
				g_i2c.status = I2C_STATUS_DATA;
			}
		}
		break;
	case I2C_STATUS_DATA:
		if( cmd->fread ) {
			rcv_data_post( cmd );
			if( rcv_data_pre( cmd ) == 1 ) {
				g_i2c.status = I2C_STATUS_COMP;
			}
		} else {
			if( snd_data( cmd ) == 0 ) {
				g_i2c.status = I2C_STATUS_COMP;
			}
		}
		break;
	case I2C_STATUS_ERR_STOP:
		*REGI2C_CR = I2C_CR_STOP;
		g_i2c.status = I2C_STATUS_ERR;
		break;
	case I2C_STATUS_ERR:
	case I2C_STATUS_COMP:
	default:
		if( cmd->fread ) {
			rcv_data_post( cmd );
		}
		// callback
		if( buf->pf != 0 ) {
			buf->pf( buf->pd, g_i2c.err );
		}
		// update
		list->num--;
		list->idx++;
		if( list->max <= list->idx ) {
			list->idx = 0;
		}
		g_i2c.status = I2C_STATUS_IDLE;
		if( g_i2c.cur_id == I2C_ID_MAX ) {
			g_i2c.cur_id = 0;
		} else {
			g_i2c.cur_id++;
		}
	}
	kick();
}

static int add_cmd( const sI2C_CMD *p, i2c_callback pf, void *pd )
{
	sI2C_CMDList *list = &( g_i2c.list );
	int ret = -1;
	if( list->num < list->max ) {
		sI2C_CMDBuf *buf;
		sI2C_CMD *cmd;
		unsigned int pos = list->idx + list->num;
		if( list->max <= pos ) {
			pos -= list->max;
		}
		buf = &( list->buf[pos] );
		cmd = &( buf->cmd );
		// set
		buf->sync_id = g_i2c.seed;
		buf->pf = pf;
		buf->pd = pd;
		// copy
		cmd->fread	= p->fread;
		cmd->saddr	= p->saddr;
		cmd->raddr	= p->raddr;
		cmd->data	= p->data;
		cmd->size	= p->size;
		// update
		list->num++;
		if( g_i2c.seed == I2C_ID_MAX ) {
			g_i2c.seed = 0;
		} else {
			g_i2c.seed++;
		}
		ret = buf->sync_id;
	}

	return ret;
}

static void init_interrupt( void )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_I2C );
	// setting ISR
	_xtos_set_interrupt_handler( INTERRUPT_NO_I2C, ( _xtos_handler )int_handler );
	// setting IMask
	_xtos_ints_on( 1 << INTERRUPT_NO_I2C );
}

/************************************
 * API Function
 ************************************/
int i2c_init( const unsigned int core_freq, const unsigned int i2c_freq )
{
	unsigned int val;
	if( core_freq == 0 || i2c_freq == 0 ) {
		return -1;
	}
	// init info
	g_i2c.status = I2C_STATUS_IDLE;
	g_i2c.err = I2C_ERR_NONE;
	g_i2c.seed = 0;
	g_i2c.cur_id = 0;
	// init command list
	g_i2c.list.max = I2C_CMD_BUFF_NUM;
	g_i2c.list.idx = 0;
	g_i2c.list.num = 0;

	// disable feature
	*REGI2C_CTR =  0x00;
	*REGI2C_CR = I2C_CR_CLRINT;
	// setting divider
	val = ( ( ( 3 * core_freq ) / i2c_freq ) - 28 ) / 16;

	val = ( 0xFFFF < val ) ? 0xFFFF : val;
	*REGI2C_PRER = val;

	// enable feature
	*REGI2C_CTR =  I2C_CTR_CORE_ENABLE | I2C_CTR_INT_ENABLE;
	// setting Interrupt
	init_interrupt();

	return 0;
}

void i2c_end( void )
{
	// disable feature
	*REGI2C_CTR =  0x00;
}

int i2c_transfer_sync( const sI2C_CMD *cmd, eI2C_ERR *err )
{
	if( cmd == 0 ) {
		return -1;
	}
	//sync
	sync( -1 );
	// lock
	_xtos_ints_off( 1 << INTERRUPT_NO_I2C );
	// set
	add_cmd( cmd, 0, 0 );
	kick();
	// unlock
	_xtos_ints_on( 1 << INTERRUPT_NO_I2C );
	//sync
	sync( -1 );
	// return with error
	if( err != 0 ) {
		*err = g_i2c.err;
	}
	if( g_i2c.err != I2C_ERR_NONE ) {
		return -2;
	} else {
		return 0;
	}
}

int i2c_transfer_add( const sI2C_CMD *cmd, i2c_callback pf, void *pd )
{
	int ret;
	if( cmd == 0 ) {
		return -1;
	}
	// lock
	_xtos_ints_off( 1 << INTERRUPT_NO_I2C );
	// set
	ret = add_cmd( cmd, pf, pd );
	if( ret < 0 ) {
		ret = -2;	// set to code which command buffer full
	} else {
		kick();
	}
	// unlock
	_xtos_ints_on( 1 << INTERRUPT_NO_I2C );
	// return result
	return ret;
}

eI2C_ERR i2c_get_error( void )
{
	return g_i2c.err;
}

void i2c_sync( int sync_id )
{
	sync( sync_id );
}

int i2c_read( unsigned char bus_addr, unsigned char data_addr, unsigned char *buff, char length )
{
	int ret;
	sI2C_CMD            cmd;
	eI2C_ERR			err;

	cmd.fread = I2C_READ;
	cmd.saddr = bus_addr;
	cmd.raddr = data_addr;
	cmd.data  = buff;
	cmd.size  = length;
	ret = i2c_transfer_sync( &cmd, 0 );
	if( ret == -2 ) {
		err = i2c_get_error();
		ret = ( int ) err;
	}

	return ret;
}

int i2c_write( unsigned char bus_addr, unsigned char data_addr, unsigned char *buff, char length )
{
	int ret;
	sI2C_CMD            cmd;
	eI2C_ERR			err;

	cmd.fread = I2C_WRITE;
	cmd.saddr = bus_addr;
	cmd.raddr = data_addr;
	cmd.data  = buff;
	cmd.size  = length;
	ret = i2c_transfer_sync( &cmd, 0 );
	if( ret == -2 ) {
		err = i2c_get_error();
		ret = ( int ) err;
	}

	return ret;
}
