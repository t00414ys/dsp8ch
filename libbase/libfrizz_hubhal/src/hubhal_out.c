/*!******************************************************************************
 * @file hubhal_out.c
 * @brief source code for hubhal_out
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/tie/frizz_simd4w_que.h>
#include "frizz_peri.h"
#include "frizz_env.h"
#include "gpio.h"
#include "hubhal_format.h"
#include "hubhal_out.h"
#include "frizz_util.h"

typedef enum {
	HUBHAL_DATA_AREA_0 = 0,
	HUBHAL_DATA_AREA_1,
	HUBHAL_DATA_AREA_NUM
} HUBHAL_DATA_AREA_IDX;

#if defined(USE_HOST_API_UART_OUT)
#include "quart.h"
#endif

hubhal_out_info_t g_hubhal_out;

#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)

static int check_fifo_space( unsigned int num )
{
	return num <= ( 64 - *REGFIFO_CNT );
}

static int snd_data( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block )
{
	unsigned int *p = ( unsigned int* )data;
	hubhal_format_header_t head;
	unsigned int i;
	int res = -1;

	// setting header
	head.prefix = HUBHAL_FORMAT_PREFIX;
	head.sen_id = sen_id;
	head.num = num + 1;
	if( area == HUBHAL_DATA_AREA_1 ) {
		head.type = HUBHAL_FORMAT_TYPE_SENSOR_DATA_SECONDARY;
	} else {
		// for default
		head.type = HUBHAL_FORMAT_TYPE_SENSOR_DATA;
	}
	if( f_block != 0 || check_fifo_space( num + 2 ) != 0 ) {
#if defined(USE_HOST_API_UART_OUT)
		// output
		quart_out_raw( "head:[0x%08X] ts:[0x%08X]", head.w, ts );
		for( i = 0; i < num; i++ ) {
			quart_out_raw( " v[%d]:[0x%08X]", i, p[i] );
		}
		quart_out_raw( "\r\n" );
#endif
		// output
		oq_sensor_out_push( head.w );
		oq_sensor_out_push( ts );
		//gpio_set_data( GPIO_NO_3, 1 );
		for( i = 0; i < num; i++ ) {
			oq_sensor_out_push( p[i] );
		}
		( GPIO_NO_3, 0 );
		res = 0;
	}

	return res;
}

static void res_data( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data )
{
	unsigned int *p = ( unsigned int* )data;
	hubhal_format_header_t head;
	unsigned int i;

	// setting header
	head.prefix = 0xFF;
	head.type = 0x84;
	head.sen_id = sen_id;
	head.num = num + 2;
#if defined(USE_HOST_API_UART_OUT)
	// output
	quart_out_raw( "head:[0x%08X] cmd:[0x%08X] res:[0x%08X]", head.w, cmd_code, res );
	for( i = 0; i < num; i++ ) {
		quart_out_raw( " v[%d]:[0x%08X]", i, p[i] );
	}
	quart_out_raw( "\r\n" );
#endif
	// output
	oq_sensor_out_push( head.w );
	oq_sensor_out_push( cmd_code );
	oq_sensor_out_push( res );
	for( i = 0; i < num; i++ ) {
		oq_sensor_out_push( p[i] );
	}
	return;
}

static void res_cmd( unsigned char sen_id, unsigned int cmd_code, int res )
{
	hubhal_format_header_t head;

	// setting header
	head.prefix = HUBHAL_FORMAT_PREFIX;
	head.type = HUBHAL_FORMAT_TYPE_RESPONSE;
	head.sen_id = sen_id;
	head.num = 2;
#if defined(USE_HOST_API_UART_OUT)
	quart_out_raw( "head:[0x%08X] cmd:[0x%08X] res:[0x%08X]\r\n", head.w, cmd_code, res );
#endif
	oq_sensor_out_push( head.w );
	oq_sensor_out_push( cmd_code );
	oq_sensor_out_push( res );
}

static void hubhal_out_isr( void )
{
	if( 0 <= g_hubhal_out.no ) {
		gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level );
	}
}

/**
 * @brief initialize
 *
 * @param no 0~n:GPIO number  <0:not use
 */
void hubhal_out_init( int no, int level )
{
	g_hubhal_out.no = no;

	gpio_init( (void*)0, 0 );
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// Interrupt Setting

	g_hubhal_out.default_level = ( level != 0 ) ? 1 : 0;

	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	_xtos_ints_off( 1 << INTERRUPT_NO_FIFO_RD );
	// transaction--->
	if( 0 <= no ) {
		gpio_set_mode( ( eGPIO_NO ) no, GPIO_MODE_OUT );
		gpio_set_data( ( eGPIO_NO ) no, g_hubhal_out.default_level );
	}
	_xtos_set_interrupt_handler( INTERRUPT_NO_FIFO_RD, ( _xtos_handler )hubhal_out_isr );
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_FIFO_RD );
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
}

/**
 * @brief output data
 *
 * @param sen_id sensor ID
 * @param area sensor area (0 or 1)
 * @param ts timestamp
 * @param num number of data
 * @param data data
 * @param f_block blocking flag(0:disable, !0:enable)
 * @param f_notify send interrupt signal flag(0:disable, !0:enable)
 *
 * @return 0: Success, <0: Fail
 */
int hubhal_out_snd_data( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block, int f_notify )
{
	int res;
	//gpio_set_data( GPIO_NO_3, 1 );
	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	// transaction--->
	gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level );
	res = snd_data( sen_id, area, ts, num, data, f_block );
	if( f_notify != 0 ) {
		gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level ^ 0x1 );
	}
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
	//gpio_set_data( GPIO_NO_3, 0 );
	return res;
}

/**
 * @brief output response for command
 *
 * @param sen_id sensor ID
 * @param cmd_code CMD Code
 * @param res Response Code
 * @param num Response Data Size
 * @param data Response Data
 */
void hubhal_out_res_cmd( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	// transaction--->
	if( num == 0 ) {
		res_cmd( sen_id, cmd_code, res );
	} else {
		res_data( sen_id, cmd_code, res, num, data );
	}
	gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level ^ 0x1 );
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
}

/**
 * @brief ACK
 *
 * @param f_ack 0:NACK, !0:ACK
 *
 * @note use only interrupt handler for critical section
 */
void hubhal_out_ack( int f_ack )
{
	if( f_ack ) {
#if defined(USE_HOST_API_UART_OUT)
		quart_out_raw( "ACK:[0x%08X]\r\n", HUBHAL_FORMAT_ACK_CODE );
#endif
		oq_sensor_out_push( HUBHAL_FORMAT_ACK_CODE );
	} else {
#if defined(USE_HOST_API_UART_OUT)
		quart_out_raw( "ACK:[0x%08X]\r\n", HUBHAL_FORMAT_NACK_CODE );
#endif
		oq_sensor_out_push( HUBHAL_FORMAT_NACK_CODE );
	}
	gpio_set_data( ( eGPIO_NO )g_hubhal_out.no, g_hubhal_out.default_level ^ 0x1 );
}
#endif	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)



int get_fifo_cnt( void )
{
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
	return *REGFIFO_CNT;
#else
	return 0;
#endif
}

#ifdef _DEBUG_
int main( void )
{
	unsigned int data[3] = { 1, 2, 3};
	output_data( 0x12, 0xdeadbeef, 3, ( void* )data );
	return 0;
}
#endif
