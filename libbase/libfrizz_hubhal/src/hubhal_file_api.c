/*!******************************************************************************
 * @file hubhal_file_api.c
 * @brief source code for hubhal_host_api
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/time.h>
#include "frizz_env.h"
#include "frizz_util.h"
#include "hubhal_in.h"
#include "hubhal_out.h"
#include "hubhal_out_txt.h"
#include "hubhal_in_state.h"
#include "hubhal_clk_calibrate.h"
#include "hubhal_block.h"
#include "base_driver.h"
#include "frizz_env.h"

#ifndef	D_FRIZZ_TICK_NS
#define	D_FRIZZ_TICK_NS		(1000000)		// 1ms = 1000000ns
#endif

#ifndef	D_FRIZZ_LOOP_LIMIT
#define	D_FRIZZ_LOOP_LIMIT	(10000)
#endif


static FILE						*fpCommand;
static char						read_buffer[1024];
static unsigned int				ts = 0;
static hubhal_format_header_t	head;
static unsigned int				cmd;
static unsigned int				body[256];

extern hubhal_in_info_t		g_hubhal_in;
extern hubhal_block_info_t	g_hubhal_block;

EXTERN_C void OpenCommandFile( void );
EXTERN_C int hubhal_in_get_file_cmd( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params );

/**
 * @brief uart receive inteface
 *
 * @param none
  */
int hubhal_in_get_file_cmd( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params )
{
	char							*tmp;
	int								i;
	static	int						loop_end_flag = 0;
#define	D_FILE_API_EXIT_WAIT		(10)

	if( loop_end_flag != 0 ) {
		loop_end_flag++;
		if( loop_end_flag > D_FILE_API_EXIT_WAIT ) {
			return -1;
		}
	} else {
		if( ts != 0 ) {
			if( g_hubhal_block.ts >= ts ) {
				*sen_id		= head.sen_id;
				*cmd_code	= cmd;
				*num		= head.num - 1;
				*params		= &( body[0] );
				ts = 0;
				if( cmd == 0xFFFFFFFF ) {
					loop_end_flag = 1;
					g_ExecOptions.TermFlag = 1;
					return 0;
				}
				return 1;
			}
		} else if( fpCommand != NULL ) {
			while( 1 ) {
				if( NULL != fgets( read_buffer , sizeof( read_buffer ) , fpCommand ) ) {
					LogFile_LineArrange( read_buffer , sizeof( read_buffer ) );
					if( *read_buffer != 0 ) {
						tmp = strtok( read_buffer, " " );
						if( tmp != 0 ) {
							//sscanf( tmp, "%u", &ts );
							ts = strtoul( tmp, NULL, 10 );
							tmp = strtok( NULL, " " );
							if( tmp != 0 ) {
								//sscanf( tmp, "%x", ( unsigned int * ) &head );
								*( unsigned int * ) &head = strtoul( tmp, NULL, 16 );
								tmp = strtok( NULL, " " );
								if( tmp != 0 ) {
									//sscanf( tmp, "%x", &cmd );
									cmd = strtoul( tmp, NULL, 16 );
									tmp = strtok( NULL, " " );
									if( tmp != 0 ) {
										for( i = 0 ; i < head.num - 1 ; i++ ) {
											if( tmp == 0 ) {
												break;
											}
											//sscanf( tmp, "%x", &body[i] );
											body[i] = strtoul( tmp, NULL, 16 );
											tmp = strtok( NULL, " " );
										}
									}
									printf( "[cmd in ]   ts=%u prefix=%02X type=%02X sen_id=%02X num=%02X cmd=%08X : body=%08X\n",	ts,
											 ( unsigned int ) head.prefix & 0x00FF,
											 ( unsigned int ) head.type   & 0x00FF,
											 ( unsigned int ) head.sen_id & 0x00FF,
											 ( unsigned int ) head.num    & 0x00FF,
											 cmd, body[0] );
									if( cmd == 0xFFFFFFFF ) {
										fclose( fpCommand );
										fpCommand = NULL;
										printf( "close %s\n", g_ExecOptions.LogFile_COMMAND );
									}
								}
							}
						}
					} else {
						continue;
					}
					break;
				} else {
					fclose( fpCommand );
					fpCommand = NULL;
					printf( "close %s\n", g_ExecOptions.LogFile_COMMAND );
					break;
				}
			}
		}
	}
	// do nothing
	return 0;
}


void OpenCommandFile( void )
{
	if( g_ExecOptions.LogFile_COMMAND[0] != 0 ) {
		fpCommand = fopen( g_ExecOptions.LogFile_COMMAND, "r" );
		if( fpCommand == NULL ) {
			printf( "ERROR: %s open error\n", g_ExecOptions.LogFile_COMMAND );
		} else {
			printf( "Command File %s\n", g_ExecOptions.LogFile_COMMAND );
		}
	}
}


#endif
