/*!******************************************************************************
 * @file  sensor_if.h
 * @brief sensor interface for sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SENSOR_LIST_H__
#define __SENSOR_LIST_H__

#include "sensor_if.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @brief Sensor area indexer
 */
typedef enum {
	SENSOR_AREA_0 = 0,	/// Primary
	SENSOR_AREA_1,		/// Secondary
	SENSOR_AREA_NUM		/// Delimiter
} SENSOR_AREA_INDEX;

typedef struct {
	sensor_if_t					*p_if;				/// child's interface
	int							attr;				/// attribute of requested status of active from child
	int							tick;				/// request interval from child
} sensor_child_info_t;

typedef struct {
	unsigned int				ts;			/// sensor data timestamp when got data by hub manager
	void						*data;		/// data address to store each sensor data
	int			                size;		/// flag of data that stored by hub manager 0:empty, !0:stored
} store_sensor_data_t;

typedef struct {
	sensor_if_t					*p_if;						/// sensor's interface
	sensor_child_info_t			*a_child_info;				/// child sensor array
	unsigned int				child_num;					/// child sensor num
	unsigned int				status[SENSOR_AREA_NUM];	/// sensor's status
	unsigned int				condition;					/// condition for physical sensor(now)
	unsigned int				last_condition;				/// condition for physical sensor(last)
	unsigned int				wake_ts;					/// next timestamp for next wakeup from
	unsigned int				out_ts[SENSOR_AREA_NUM];	/// next timestamp for sensor's output timing
	int							tick[SENSOR_AREA_NUM];		/// sensor's current interval (exclude child) -1:One-shot, 0:On-changed, 0<:interval tick
	store_sensor_data_t         latest[SENSOR_AREA_NUM];    /// member for latest sensor data management, 0:primary,1:secondary
} sensor_info_t;

/**
 * @brief Sensor's Status
 */
#define SENSOR_STATUS_FLAGS_OUTPUT			(1<<0)
#define SENSOR_STATUS_FLAGS_OUTPUT_DUMP		(1<<1)
#define SENSOR_STATUS_FLAGS_OUTPUT_SILENT	(1<<2)
#define SENSOR_STATUS_FLAGS_OUTPUT_WITH_INT	(1<<3)
#define SENSOR_STATUS_FLAGS_UPDATED			(1<<8)
#define SENSOR_STATUS_FLAGS_STORED_DATA		(1<<12)

#define SENSOR_STATUS_FLAGS_CHG_CONDITION	(1<<7)

typedef struct {
	sensor_info_t				**pa_sensor;		// job array by sensor_if
	unsigned int				num;
} sensor_list_linear_t;

typedef void* sensor_list_h;

void sensor_list_destroy( sensor_list_h h );

sensor_list_h sensor_list_create( void );

int sensor_list_add_sensor( sensor_list_h h, sensor_if_t *pif );

int sensor_list_fix( sensor_list_h h );

sensor_list_linear_t* sensor_list_get_linear_list( sensor_list_h h );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
