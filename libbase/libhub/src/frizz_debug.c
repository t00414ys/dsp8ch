/*!******************************************************************************
 * @file  frizz_debug.c
 * @brief source for debug I/F
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "sensor_if.h"
#include "sensor_list.h"
#include "fifo_mgr.h"
#include "hub_mgr.h"
#include "hub_mgr_if.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "frizz_debug.h"

static	unsigned char	frizz_debug_level[0x100];


/**
 * @brief frizz host debug data transmit interface.<BR>
 *        This function is Without free will to FIFO, do not send.
 *
 * @param [in] status		message status (FRIZZ_NOMSG ~ FRIZZ_DEBUG)
 * @param [in] pos			position Information
 * @param [in] buff			send data pointer
 * @param [in] length		buffer length
 * @retval	0	Success
 * @retval	-1	FIFO full
 *
 * @note ex).<BR>
 *        #include "frizz_debug.h"
 *        #define POS_GESTURE_CORE POS_MACRO(SENSOR_ID_GESTURE, 0x01);  => Unique number of this file <BR>
 *                              :                                                                     <BR>
 *        static int calculate(void)                                                                  <BR>
 *        {                                                                                           <BR>
 *          short        count,sum;                                                                   <BR>
 *          sFrizzDEBUG  data;                                                                        <BR>
 *                              :                                                                     <BR>
 *                              :                                                                     <BR>
 *          data.data[0] = 0x11111111;                                                                <BR>
 *          data.data[1] = (count << 16) | sum;                                                       <BR>
 *          data.data[2] = 0x12345678;                                                                <BR>
 *          data.data[3] = 0x00000000;                                                                <BR>
 *          frizz_debug(FRIZZ_WARNING,POS_GESTURE_CORE,&data,sizeof(data));                           <BR>
 */
int frizz_debug( unsigned char status, unsigned long pos, sFrizzDEBUG *data )
{
	unsigned char	snd_data[28];
	unsigned char	SendId = ( pos & 0xFF000000 ) >> 24;
	if( frizz_debug_level[SendId] >= status ) {
		snd_data[0]  = 0x06;		//< payload word num
		snd_data[1]  = SendId;		//< sensor ID
		snd_data[2]  = 0x80;		//< 0x80: SensorOutput
		snd_data[3]  = 0xFF;		//< 0xFF: prefix
		snd_data[4]  = 0x00;
		snd_data[5]  = frizz_debug_level[SendId];
		snd_data[6]  = SendId;
		snd_data[7]  = HUB_MGR_CMD_PUSH_DEBUG_DATA;
		*( unsigned int * )&snd_data[8]  = pos;
		*( unsigned int * )&snd_data[12] = data->data[0];
		*( unsigned int * )&snd_data[16] = data->data[1];
		*( unsigned int * )&snd_data[20] = data->data[2];
		*( unsigned int * )&snd_data[24] = data->data[3];
		hub_data_output( sizeof( snd_data ) / sizeof( int ), ( void* )&snd_data[0] );
	}
	return 0;
}


/**
 * @brief set frizz host debug level.<BR>
 *
 * @param [in] sen_id		sensor id
 * @param [in] level		debug level
 * @retval	void
 *
 */
void frizz_debug_level_set( unsigned char sen_id, unsigned char level )
{
	if( level <= FRIZZ_DEBUG ) {
		frizz_debug_level[sen_id] = level;
	}
	return;
}


/**
 * @brief get frizz host debug level.<BR>
 *
 * @param [in] sen_id		sensor id
 * @retval	debug level
 */
unsigned char frizz_debug_level_get( unsigned char sen_id )
{
	return 	frizz_debug_level[sen_id];
}

