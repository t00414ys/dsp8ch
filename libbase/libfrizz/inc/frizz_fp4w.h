/*!******************************************************************************
 * @file  frizz_fp4w.h
 * @brief Frizz Float Type Header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_FP4W_H__
#define __FRIZZ_FP4W_H__
#include "frizz_type.h"

#if !defined(RUN_ON_PC)
// Xtensa Platform
#include <xtensa/tie/frizz_simd4w_que.h>
#ifndef FRIZZ_TIE_VER00002000
#include "frizz_op4w.h"	// for upper compatible
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef frizz_tie_fp4w frizz_fp4w;

#ifdef __cplusplus
}
#endif

#else
// Other Platform
class frizz_fp4w
{
	friend frizz_fp4w frizz_tie_store_const4w( const frizz_fp& o );
	friend frizz_fp frizz_tie_vreduc( const frizz_fp4w& o );
	friend void frizz_tie_vshift_l( frizz_fp &pop, frizz_fp4w &o, const frizz_fp in );
	friend void frizz_tie_vshift_r( const frizz_fp in, frizz_fp4w &o, frizz_fp &pop );
	friend frizz_fp4w frizz_tie_vneg( frizz_fp4w a, int imm );
	friend frizz_fp frizz_tie_add( frizz_fp s0, frizz_fp s1 );
	friend frizz_fp frizz_tie_sub( frizz_fp s0, frizz_fp s1 );
	friend frizz_fp frizz_tie_mul( frizz_fp s0, frizz_fp s1 );
	friend void frizz_tie_mac( frizz_fp &d0, frizz_fp m0, frizz_fp s0 );
	friend int frizz_tie_cmplt( frizz_fp s0, frizz_fp s1 );
	friend int frizz_tie_cmple( frizz_fp s0, frizz_fp s1 );
	friend int frizz_tie_cmpeq( frizz_fp s0, frizz_fp s1 );
	friend int frizz_tie_cmpge( frizz_fp s0, frizz_fp s1 );
	friend int frizz_tie_cmpgt( frizz_fp s0, frizz_fp s1 );
	friend int frizz_tie_cmpne( frizz_fp s0, frizz_fp s1 );
private:
	frizz_fp	v[4];

public:
	// assignment operator
	inline frizz_fp4w& operator = ( const frizz_fp& f );
	// unary operator
	inline frizz_fp4w& operator += ( const frizz_fp4w& o );
	inline frizz_fp4w& operator -= ( const frizz_fp4w& o );
	inline frizz_fp4w& operator *= ( const frizz_fp4w& o );

	inline frizz_fp4w& operator += ( const frizz_fp& f );
	inline frizz_fp4w& operator -= ( const frizz_fp& f );
	inline frizz_fp4w& operator *= ( const frizz_fp& f );
	// dyadic operator
	inline const frizz_fp4w operator + ( const frizz_fp4w& o ) const;
	inline const frizz_fp4w operator - ( const frizz_fp4w& o ) const;
	inline const frizz_fp4w operator * ( const frizz_fp4w& o ) const;
	inline const frizz_fp4w operator - () const;

	inline const frizz_fp4w operator + ( const frizz_fp& f ) const;
	inline const frizz_fp4w operator - ( const frizz_fp& f ) const;
	inline const frizz_fp4w operator * ( const frizz_fp& f ) const;
};

// init
#define frizz_tie_setzero4w	frizz_fp4w
// dyadic operator
inline frizz_fp4w operator + ( const frizz_fp& f, const frizz_fp4w& o )
{
	return ( o + f );
}
inline frizz_fp4w operator - ( const frizz_fp& f, const frizz_fp4w& o )
{
	return -( o - f );
}
inline frizz_fp4w operator * ( const frizz_fp& f, const frizz_fp4w& o )
{
	return ( o * f );
}
inline frizz_fp frizz_tie_vreduc( const frizz_fp4w& o )
{
	return ( o.v[0] + o.v[1] + o.v[2] + o.v[3] );
}
inline void frizz_tie_vshift_l( frizz_fp &pop, frizz_fp4w &o, const frizz_fp in )
{
	frizz_fp src = in;
	// caution: little endian
	pop = o.v[3];
	o.v[3] = o.v[2];
	o.v[2] = o.v[1];
	o.v[1] = o.v[0];
	o.v[0] = src;
}
inline void frizz_tie_vshift_r( const frizz_fp in, frizz_fp4w &o, frizz_fp &pop )
{
	frizz_fp src = in;
	// caution: little endian
	pop = o.v[0];
	o.v[0] = o.v[1];
	o.v[1] = o.v[2];
	o.v[2] = o.v[3];
	o.v[3] = src;
}

// store
inline frizz_fp4w frizz_tie_store_const4w( const frizz_fp& o )
{
	frizz_fp4w w;
	w = o;
	return w;
}

inline frizz_fp4w frizz_tie_vneg( frizz_fp4w a, int imm )
{
	a.v[0] = ( imm & 1 ) ? -a.v[0] : a.v[0];
	a.v[1] = ( imm & 2 ) ? -a.v[1] : a.v[1];
	a.v[2] = ( imm & 4 ) ? -a.v[2] : a.v[2];
	a.v[3] = ( imm & 8 ) ? -a.v[3] : a.v[3];
	return a;
}

inline frizz_fp frizz_tie_add( frizz_fp s0, frizz_fp s1 )
{
	return s0 + s1;
}
inline frizz_fp frizz_tie_sub( frizz_fp s0, frizz_fp s1 )
{
	return s0 - s1;
}
inline frizz_fp frizz_tie_mul( frizz_fp s0, frizz_fp s1 )
{
	return s0 * s1;
}
inline void frizz_tie_mac( frizz_fp &d0, frizz_fp m0, frizz_fp s0 )
{
	d0 += m0 * s0;
}
inline int frizz_tie_cmplt( frizz_fp s0, frizz_fp s1 )
{
	return s0 <  s1;
}
inline int frizz_tie_cmple( frizz_fp s0, frizz_fp s1 )
{
	return s0 <= s1;
}
inline int frizz_tie_cmpeq( frizz_fp s0, frizz_fp s1 )
{
	return s0 == s1;
}
inline int frizz_tie_cmpge( frizz_fp s0, frizz_fp s1 )
{
	return s0 >= s1;
}
inline int frizz_tie_cmpgt( frizz_fp s0, frizz_fp s1 )
{
	return s0 >  s1;
}
inline int frizz_tie_cmpne( frizz_fp s0, frizz_fp s1 )
{
	return s0 != s1;
}

#if defined(ENABLE_SSE)
#include <mmintrin.h>
#include <xmmintrin.h>
/***** SSE *****/
// vector
inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp4w& o )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_add_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp4w& o )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_sub_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp4w& o )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_mul_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_add_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_sub_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load_ps( ( float* )( o.v ) );
	imd0 = _mm_mul_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - () const
{
	float zero = 0.0;
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load1_ps( &zero );
	__m128 ims1 = _mm_load_ps( ( float* )( v ) );
	imd0 = _mm_sub_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

// scale & vector
inline frizz_fp4w& frizz_fp4w::operator = ( const frizz_fp& f )
{
	__m128 imd0 = _mm_load1_ps( ( float* )( &f ) );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp& f )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_add_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp& f )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_sub_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp& f )
{
	__m128 imd0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims0 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_mul_ps( imd0, ims0 );
	_mm_store_ps( ( float* )( v ), imd0 );
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_add_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_sub_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	__m128 imd0;
	__m128 ims0 = _mm_load_ps( ( float* )( v ) );
	__m128 ims1 = _mm_load1_ps( ( float* )( &f ) );
	imd0 = _mm_mul_ps( ims0, ims1 );
	_mm_store_ps( ( float* )( tmp.v ), imd0 );
	return tmp;
}

#elif defined(ENABLE_NEON)
#include "arm_neon.h"
/***** NEON *****/
// vector
inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp4w& o )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vaddq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp4w& o )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vsubq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp4w& o )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vmulq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vaddq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vsubq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vld1q_f32( ( float* )( o.v ) );
	imd0 = vmulq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - () const
{
	float zero = 0.0;
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vdupq_n_f32( &zero );
	float32x4_t ims1 = vld1q_f32( ( float* )( v ) );
	imd0 = vsubq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

// scale & vector
inline frizz_fp4w& frizz_fp4w::operator = ( const frizz_fp& f )
{
	float32x4_t imd0 = vdupq_n_f32( ( float* )( &f ) );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp& f )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vaddq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp& f )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vsubq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp& f )
{
	float32x4_t imd0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims0 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vmulq_f32( imd0, ims0 );
	vst1q_f32( ( float* )( v ), imd0 );
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vaddq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vsubq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	float32x4_t imd0;
	float32x4_t ims0 = vld1q_f32( ( float* )( v ) );
	float32x4_t ims1 = vdupq_n_f32( ( float* )( &f ) );
	imd0 = vmulq_f32( ims0, ims1 );
	vst1q_f32( ( float* )( tmp.v ), imd0 );
	return tmp;
}

#else
/***** Sequential *****/
// vector
inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp4w& o )
{
	v[0] += o.v[0];
	v[1] += o.v[1];
	v[2] += o.v[2];
	v[3] += o.v[3];
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp4w& o )
{
	v[0] -= o.v[0];
	v[1] -= o.v[1];
	v[2] -= o.v[2];
	v[3] -= o.v[3];
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp4w& o )
{
	v[0] *= o.v[0];
	v[1] *= o.v[1];
	v[2] *= o.v[2];
	v[3] *= o.v[3];
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] + o.v[0];
	tmp.v[1] = v[1] + o.v[1];
	tmp.v[2] = v[2] + o.v[2];
	tmp.v[3] = v[3] + o.v[3];
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] - o.v[0];
	tmp.v[1] = v[1] - o.v[1];
	tmp.v[2] = v[2] - o.v[2];
	tmp.v[3] = v[3] - o.v[3];
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp4w& o ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] * o.v[0];
	tmp.v[1] = v[1] * o.v[1];
	tmp.v[2] = v[2] * o.v[2];
	tmp.v[3] = v[3] * o.v[3];
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - () const
{
	frizz_fp4w tmp;
	tmp.v[0] = -v[0];
	tmp.v[1] = -v[1];
	tmp.v[2] = -v[2];
	tmp.v[3] = -v[3];
	return tmp;
}

// scale & vector
inline frizz_fp4w& frizz_fp4w::operator = ( const frizz_fp& f )
{
	v[0] =
		v[1] =
			v[2] =
				v[3] = f;
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator += ( const frizz_fp& f )
{
	v[0] += f;
	v[1] += f;
	v[2] += f;
	v[3] += f;
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator -= ( const frizz_fp& f )
{
	v[0] -= f;
	v[1] -= f;
	v[2] -= f;
	v[3] -= f;
	return *this;
}

inline frizz_fp4w& frizz_fp4w::operator *= ( const frizz_fp& f )
{
	v[0] *= f;
	v[1] *= f;
	v[2] *= f;
	v[3] *= f;
	return *this;
}

inline const frizz_fp4w frizz_fp4w::operator + ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] + f;
	tmp.v[1] = v[1] + f;
	tmp.v[2] = v[2] + f;
	tmp.v[3] = v[3] + f;
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator - ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] - f;
	tmp.v[1] = v[1] - f;
	tmp.v[2] = v[2] - f;
	tmp.v[3] = v[3] - f;
	return tmp;
}

inline const frizz_fp4w frizz_fp4w::operator * ( const frizz_fp& f ) const
{
	frizz_fp4w tmp;
	tmp.v[0] = v[0] * f;
	tmp.v[1] = v[1] * f;
	tmp.v[2] = v[2] * f;
	tmp.v[3] = v[3] * f;
	return tmp;
}
#endif	// Other Platform Select

#endif	// Xtensa or Other

typedef union {
		frizz_fp4w	w;
		frizz_fp	z[4];
		float		f[4];
	} frizz_fp4w_t;

#endif	// __FRIZZ_FP4W_H__

