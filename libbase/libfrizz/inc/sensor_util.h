/*!******************************************************************************
 * @file  sensor_util.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SENSOR_UTIL_H__
#define __SENSOR_UTIL_H__

#include "frizz_type.h"

typedef union {
	unsigned short	uh;
	short			half;
	unsigned char	ubyte[2];
} sensor_util_half_t;

#endif // __SENSOR_UTIL_H__
