/*!******************************************************************************
 * @file  frizz_op.h
 * @brief Operator Overload for Frizz Float Type
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_OP_H__
#define __FRIZZ_OP_H__

#if !defined(RUN_ON_PC)
#include <xtensa/tie/frizz_simd4w_que.h>

typedef union {
	frizz_tie_fp	fr;
	float			fp;
} _frizz_op_union;

/* overload operator */
// negate
static inline frizz_tie_fp __xt_operator_NEGATE( frizz_tie_fp a )
{
	_frizz_op_union u;
	u.fp = 0.0;
	return frizz_tie_sssub( u.fr, a );
}
// +
static inline frizz_tie_fp __xt_operator_PLUS( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_ssadd( a, b );
}
// -
static inline frizz_tie_fp __xt_operator_MINUS( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_sssub( a, b );
}
// *
static inline frizz_tie_fp __xt_operator_MULT( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_ssmul( a, b );
}

// <
static inline int __xt_operator_LT( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_ltcmp( a, b );
}
// <=
static inline int __xt_operator_LE( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_lecmp( a, b );
}
// >
static inline int __xt_operator_GT( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_gtcmp( a, b );
}
// >=
static inline int __xt_operator_GE( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_tie_gecmp( a, b );
}
// ==
static inline int __xt_operator_EQ( frizz_tie_fp a, frizz_tie_fp b )
{
	return ( a >= b ) && ( a <= b );
}
// !=
static inline int __xt_operator_NE( frizz_tie_fp a, frizz_tie_fp b )
{
	return ( a < b ) || ( a > b );
}

#endif	// !defined(RUN_ON_PC)

#endif	// __FRIZZ_OP_H__

