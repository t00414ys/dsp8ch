/*!******************************************************************************
 * @file  kalman_filter.h
 * @brief For Kalman Filter on frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __KALMAN_FILTER_H__
#define __KALMAN_FILTER_H__
#include "frizz_type.h"
#include "matrix.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void* KF_h;

/**
 * @brief callback at KF_Init
 *
 * @param arg argument
 * @param h KalmanFilter handler
 */
typedef void ( *kf_init )( void* arg, KF_h h );

/**
 * @brief callback at KF_Delete
 *
 * @param arg argument
 */
typedef void ( *kf_del )( void* arg );

/**
 * @brief callback at KF_Predict
 *
 * @param arg argument
 * @param h KalmanFilter handler
 * @param con vector as control-input
 * @param dtm time of sampling interval
 */
typedef void ( *kf_pre )( void* arg, KF_h h, frizz_fp *con, frizz_fp dtm );

/**
 * @brief structure for callback
 */
typedef struct {
	void*		arg;		//!< argument
	kf_init		init_func;	//!< at KF_Init
	kf_del		del_func;	//!< at KF_Delete
	kf_pre		pre_func;	//!< at KF_Predict
} KF_Callback;

/**
 * @brief Create KalmanFilter handler
 *
 * @param stv_num num of states
 * @param mes_num num of measurements
 * @param con_num num of control-inputs
 * @param p_cb structure for callback
 *
 * @return KalmanFilter handler
 *
 * @note Please refer & set parameters with KF_Get*()
 */
KF_h KF_New( int stv_num, int mes_num, int con_num, KF_Callback *p_cb );

/**
 * @brief Initilize
 *
 * @param h KalmanFilter handler
 */
void KF_Init( KF_h h );

/**
 * @brief Delete KalmanFilter handler
 *
 * @param h KalmanFilter handler
 */
void KF_Delete( KF_h h );

/**
 * @brief refer vector (matrix) of states
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of states
 */
Matrix* KF_GetSTV( KF_h h );

/**
 * @brief refer vector (matrix) of predicted states at stage of prediction
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of predicted states
 */
Matrix* KF_GetPRD( KF_h h );

/**
 * @brief refer vector (matrix) of residual between measurements and predictons
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of residual
 */
Matrix* KF_GetRES( KF_h h );

/**
 * @brief refer Phi: Transition matrix
 *
 * @param h KalmanFilter handler
 *
 * @return Phi matrix
 *
 * @note Phi = (I+dtF)
 */
Matrix* KF_GetPHI( KF_h h );

/**
 * @brief refer G: Control-input transition matrix
 *
 * @param h KalmanFilter handler
 *
 * @return G matrix
 *
 * @note prd = (I+dtF) * stv + G * input
 */
Matrix* KF_GetG( KF_h h );

/**
 * @brief refer H: Measurement matrix
 *
 * @param h KalmanFilter handler
 *
 * @return H matri
 *
 * @note mes = H * stv
 */
Matrix* KF_GetH( KF_h h );

/**
 * @brief refer P: Error covariance matrix
 *
 * @param h KalmanFilter handler
 *
 * @return P matrix
 */
Matrix* KF_GetP( KF_h h );

/**
 * @brief refer Q: Covariance matrix for noise of process at discrete time domain
 *
 * @param h KalmanFilter handler
 *
 * @return Q matrix
 */
Matrix* KF_GetQ( KF_h h );

/**
 * @brief refer R: Covariance matrix for noise of measurement
 *
 * @param h KalmanFilter handler
 *
 * @return matrix[row][col]
 */
Matrix* KF_GetR( KF_h h );

/**
 * @brief refer S: Covariance matrix for noise of control-input
 *
 * @param h KalmanFilter handler
 *
 * @return S matrix
 */
Matrix* KF_GetS( KF_h h );

/**
 * @brief refer Identity matrix (num of states x num of states)
 *
 * @param h KalmanFilter handler
 *
 * @return Identity matrix
 */
Matrix* KF_GetI( KF_h h );

/**
 * @brief make covariance matrix for noise of process at discrete time domain from countinuous time domain
 *
 * @param h KalmanFilter handler
 * @param F_m Dynamics matrix (stv' = F_m * stv)
 * @param Q_c Q matrix at continuous time domain (Q_c = E[w * wt])
 * @param dtm time of sampling interval
 */
void KF_MakeQ( KF_h h, Matrix* F_m, Matrix* Q_c, frizz_fp dtm );

/**
 * @brief prediction
 *
 * @param h KalmanFilter handler
 * @param con control-input (not use ,if NULL)
 * @param dtm time of sampling interval
 */
void KF_Predict( KF_h h, frizz_fp* con, frizz_fp dtm );

/**
 * @brief make residual between measurements and predictons
 *
 * @param h KalmanFilter handler
 * @param mes measurement
 */
void KF_MakeResidual( KF_h h, frizz_fp *mes );

/**
 * @brief filtering
 *
 * @param h KalmanFilter handler
 */
void KF_Update( KF_h h );

#ifdef __cplusplus
}
#endif

#endif//__KALMAN_FILTER_H__
