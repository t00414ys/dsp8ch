/*!******************************************************************************
 * @file frizz_const.c
 * @brief const value table
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_const.h"

static const float _frizz_const[] = {
	0.0,	// zero
	1.0,	// one
	2.0,	// two
	0.5,	// half
	0.25,	// quarter
	1000,	// thousand
	360.0,	// 360
	180.0,  // 180
	90.0    // 90
};
const frizz_fp *frizz_const = ( const frizz_fp* )_frizz_const;

