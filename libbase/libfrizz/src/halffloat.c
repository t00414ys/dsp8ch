/*!******************************************************************************
 * @file halffloat.c
 * @brief MCC Library for halffloat
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "halffloat.h"

// Half float values
#define	D_SIGNMASK		(0x8000)
#define	D_EXPMASK		(0x7C00)
#define	D_MANTMASK		(0x03FF)
#define	D_HIDDENBIT		(0x0400)

// float values
#define	D_FSIGNMASK		(0x80000000)
#define	D_FEXPMASK		(0x7F800000)
#define	D_FMANTMASK		(0x007FFFFF)
#define	D_FHIDDENBIT	(0x00800000)

#define	D_F_INFINITY	(0x7F800000)
#define	D_F_NON			(0x7Fc00000)

// Rounding mode
#define	D_TONEAREST
//#define	D_UPWARD
//#define	D_DOWNWARD
//#define	D_TOZERO



/**
 * @brief floatToShort
 *
 * @param float f
 * @param unsigned short data
 **/
unsigned short floatToShort( float f )
{
	unsigned int		s		= *( unsigned int * )( void * )&f;
	unsigned short		u		= ( s & D_FSIGNMASK ) ? D_SIGNMASK : 0;
	int					exp		= s & D_FEXPMASK;
	unsigned int		significand;
	int					guard;
	int					sticky;
	unsigned int		shift;

	// if nan or infinity
	if( exp == D_FEXPMASK ) {
		if( ( s & D_FMANTMASK ) == 0 ) {	//  if infinity
			u |= D_EXPMASK;
		} else {						// nan
			u |= D_EXPMASK | 1;
		}
		return u;
	}

	significand = s & D_FMANTMASK;

	if( exp == 0 ) {					// if subnormal or zero
		// if zero
		if( significand == 0 ) {
			return u;
		}
		/* A subnormal float is going to give us a zero result anyway,
		 * so just set UNDERFLOW and INEXACT and return +-0.
		*/
		return u;
	} else {							// else normal
		// normalize exponent and remove bias
		exp = ( exp >> 23 ) - 127;
		significand |= D_FHIDDENBIT;
	}

	exp		+= 15;						// bias the exponent

	guard	= 0;						// guard bit
	sticky	= 0;    					// sticky bit
	shift	= 13;						// lop off rightmost 13 bits

	if( exp <= 0 ) {					// if subnormal
		shift += -exp + 1;				// more bits to lop off
		exp = 0;
	}

	if( shift > 23 ) {
		// Set UNDERFLOW, INEXACT, return +-0
		return u;
	}

	// Lop off rightmost 13 bits, but save guard and sticky bits
	guard = ( significand & ( 1 << ( shift - 1 ) ) ) != 0;
	sticky = ( significand & ( ( 1 << ( shift - 1 ) ) - 1 ) ) != 0;
	significand >>= shift;

	if( guard || sticky ) {
		// Lost some bits, so set INEXACT and round the result
#if		defined(D_TONEAREST)
		if( guard && ( sticky || ( significand & 1 ) ) ) {
			++significand;
		}
#endif
#if 0
		//#elif	defined(D_UPWARD)
		if( !( s & D_FSIGNMASK ) ) {
			++significand;
		}
		//#elif	defined(D_DOWNWARD)
		if( s & D_FSIGNMASK ) {
			++significand;
		}
		//#elif	defined(D_TOZERO)
#endif
		// if subnormal
		if( exp == 0 ) {
			// and not a subnormal no more
			if( significand & D_HIDDENBIT ) {
				++exp;
			}
		} else if( significand & ( D_HIDDENBIT << 1 ) ) {
			significand >>= 1;
			++exp;
		}
	}

	// Set OVERFLOW and INEXACT, return +-infinity
	if( exp > 30 ) {
		return u | D_EXPMASK;
	}

	/* Add exponent and significand into result.*/

	u |= exp << 10;									// exponent
	u |= ( significand & ~D_HIDDENBIT );				// significand

	return u;
}


float shortToFloat( unsigned short s )
{
	int				exp		= s & D_EXPMASK;
	float			f		= 0;
	unsigned int	*pf		= ( unsigned int * )( void * ) &f;
	unsigned int	significand;
	unsigned int	u;

	// if nan or infinity
	if( exp == D_EXPMASK ) {
		// if infinity
		if( ( s & D_MANTMASK ) == 0 ) {
			*pf = D_F_INFINITY;
		} else {									// nan
			*pf = D_F_NON;
		}
		return ( s & D_SIGNMASK ) ? -f : f;
	}

	significand = s & D_MANTMASK;

	// if subnormal or zero
	if( exp == 0 ) {
		// if zero
		if( significand == 0 ) {
			return ( s & D_SIGNMASK ) ? -0.0f : 0.0f;
		}

		// Normalize by shifting until the hidden bit is 1
		while( !( significand & D_HIDDENBIT ) ) {
			significand <<= 1;
			--exp;
		}
		significand &= ~D_HIDDENBIT;					// hidden bit is, well, hidden
		exp -= 14;
	} else {										// else normal
		// normalize exponent and remove bias
		exp = ( exp >> 10 ) - 15;
	}

	/* Assemble sign, exponent, and significand into float.
	 * Don't have to deal with overflow, inexact, or subnormal
	 * because the range of floats is big enough.
	*/

	//assert(-126 <= exp && exp <= 127);			// just to be sure

	u = ( s & D_SIGNMASK ) << 16;						// sign bit
	u |= ( exp + 127 ) << 23;							// bias the exponent and shift into position
	u |= significand << ( 23 - 10 );

	return *( float* )( void * )&u;
}

