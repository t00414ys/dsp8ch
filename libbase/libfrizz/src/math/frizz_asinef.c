/*!******************************************************************************
 * @file frizz_asinef.c
 * @brief frizz_asinef() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"
#include "frizz_asinef.h"

/**
 * @brief constant value for frizz_asinef
 */
static const float asinef_r[] = {
	// 0:z_rooteps_f
	1.7263349182589107e-4,
	// 1:p
	0.933935835, -0.504400557,
	// 3:q
	0.560363004e+1, -0.554846723e+1,
	// 5:a
	0.0, 0.785398163,
	// 7:b
	1.570796326, 0.785398163,
};

/**
 * @brief arc sine / cosine
 *
 * @param [in] x
 * @param [in] acosine 0: arc sine, 1:arc cosine
 *
 * @return arc sine or cosine
 */
frizz_fp frizz_asinef( frizz_fp x, int acosine )
{
	const frizz_fp *r = ( const frizz_fp* )asinef_r;
	int flag, i;
	int branch = 0;
	frizz_fp g, res, R, P, Q, y;

	// for warning
	g = res = CONST_ZERO;
	/* Check for special values. */
	{
		_MathFPCast c_x;
		c_x.zp = x;
		if( c_x.fbit.exp == 255 ) {
			return x;
		}
	}

	y = frizz_fabs( x );
	flag = acosine;

	if( CONST_HALF < y ) {
		i = 1 - flag;

		/* Check for range error. */
		if( CONST_ONE < y ) {
			_MathFPCast c_res;
			c_res.ui = 0x7FC00000;
			return c_res.zp;
		}

		g = frizz_div( ( CONST_ONE - y ), CONST_TWO );
		y = -CONST_TWO * frizz_sqrt( g );
		branch = 1;
	} else {
		i = flag;
		if( y < r[0] ) {
			res = y;
		} else {
			g = y * y;
		}
	}

	if( r[0] <= y || branch == 1 ) {
		/* Calculate the Taylor series. */
		P = ( r[1 + 1] * g + r[1 + 0] ) * g;
		Q = ( g + r[3 + 1] ) * g + r[3 + 0];
		R = frizz_div( P, Q );

		res = y + y * R;
	}

	/* Calculate asine or acose. */
	if( flag == 0 ) {
		res = ( r[5 + i] + res ) + r[5 + i];
		if( x < CONST_ZERO ) {
			res = -res;
		}
	} else {
		if( x < CONST_ZERO ) {
			res = ( r[7 + i] + res ) + r[7 + i];
		} else {
			res = ( r[5 + i] - res ) + r[5 + i];
		}
	}

	return ( res );
}

