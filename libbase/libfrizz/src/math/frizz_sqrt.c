/*!******************************************************************************
 * @file frizz_sqrt.c
 * @brief frizz_sqrt() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief constant value for frizz_sqrt
 */
static const float sqrt_r[] = {
	// y = a * x + b
	-0.29289321881345247559915563789515,
	( 1.2928932188134524755991556378952 - 0.02 ),
	// A(n+1) = A(n) * (1.5 - 0.5 * x * A(n)^2)
	1.5,
	// root(2.0)
	1.4142135623730950488016887242097,
};

/**
 * @brief square root
 *
 * @param [in] in
 *
 * @return value of square root of input
 */
frizz_fp frizz_sqrt( frizz_fp x )
{
	_MathFPCast y, f, ff;
	const frizz_fp *r = ( const frizz_fp* )sqrt_r;
	int i, exp;

	if( x == CONST_ZERO ) {
		return CONST_ZERO;
	} else if( x < CONST_ZERO ) {
		f.zp = x;
		f.fbit.exp = 255;
		f.fbit.frac = 0x400000;
	}
	/* Find the exponent and mantissa for the form x = f * 2^exp. */
	y.zp = x;
	exp = y.fbit.exp;
	if( exp == 0 ) {
		for( exp = 1; y.fbit.exp == 0; y.ui <<= 1, exp-- );
	}
	exp -= 127;
	y.fbit.exp = 127;

	/* Get the initial approximation. */
	f.zp = r[0] * y.zp + r[1];
	//	f = 0.81649658092772603273242802490196f;

	/* Calculate the remaining iterations. */
	y.zp *= CONST_HALF;
	for( i = 0; i < 3; i++ ) {
		ff.zp = f.zp * f.zp;
		// A(n+r) = A(n) * (1.5 - 0.5 * x * A(n)^2) for 1/sqrt(x)
		f.zp = f.zp * ( r[2] - y.zp * ff.zp );
	}

	if( exp & 1 ) {
		f.zp *= r[3];
		exp++;
	}
	f.fbit.exp = f.fbit.exp - ( exp >> 1 );

	return ( x * f.zp );
}

