/*!******************************************************************************
 * @file  frizz_math_common.h
 * @brief common define for frizz_math
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_const.h"

/// caution endian to bit reference
/**
 * @brief union for cast
 */
typedef union {
	frizz_fp		zp;
	unsigned int	ui;
	int				i;
	struct {
		unsigned int frac:	23;
		unsigned int exp:	 8;
		unsigned int sign:	 1;
	} fbit;
} _MathFPCast;

#define CONST_ZERO		FRIZZ_CONST_ZERO
#define CONST_ONE		FRIZZ_CONST_ONE
#define CONST_TWO		FRIZZ_CONST_TWO
#define CONST_HALF		FRIZZ_CONST_HALF
#define CONST_QUARTER	FRIZZ_CONST_QUARTER

