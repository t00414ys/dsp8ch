/*!******************************************************************************
 * @file    hy3118_api.h
 * @brief   hy3118 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADC_HY3118_API_H__
#define __ADC_HY3118_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int adc_hy3118_init( unsigned int param );
void adc_hy3118_ctrl( int f_ena );
unsigned int adc_hy3118_rcv( unsigned int tick );
int adc_hy3118_conv( frizz_fp* adc_data );

int adc_hy3118_setparam( void *ptr );
unsigned int adc_hy3118_get_ver( void );
unsigned int adc_hy3118_get_name( void );

int adc_hy3118_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
