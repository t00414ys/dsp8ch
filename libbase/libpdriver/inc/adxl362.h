/*!******************************************************************************
 * @file    adxl362.h
 * @brief   adxl362 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          ADXL362 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_H__
#define __ADXL362_H__
#include "adxl362_api.h"

/*
 *
 *		By environment, you must edit this section.
 *
 */

/* GPIO number to be assigned to the chip select signal */
#define ADXL362_SPI_CS_NO			(0)
#define ADXL362_SPI_FREQ			(400000)


/*
 *		Please enable definition of each to enable want value.
 *		Please comment out the rest of the definition.
 */

/* antialiasing filters bandwidth */
/* Measurement Range */
//#define ACC_2G_RANGE
//#define ACC_4G_RANGE
#define ACC_8G_RANGE

//#define ACC_BW_QUARTER
#define ACC_BW_HALF

/* Output data rate */
//#define ACC_12R5HZ_ODR
//#define ACC_25HZ_ODR
//#define ACC_50HZ_ODR
#define ACC_100HZ_ODR
//#define ACC_200HZ_ODR
//#define ACC_400HZ_ODR

/* Noise */
#define ACC_NORMAL_NOISE
//#define ACC_LOW_NOISE
//#define ACC_ULTRALOW_NOISE


/*
 *
 * You do not need to change ahead from here
 *
 */

/* Device ID registers */
#define ADXL362_DEVID_AD_REG_ADD (0x00)

#define ADXL362_DEVID_AD_REG_VAL (0xad)
#define ADXL362_DEVID_MST_REG_VAL (0x1d)
#define ADXL362_PARTID_REG_VAL (0xf2)

//-----Filter control register-----
#define ADXL362_FILTER_CTL_REG_ADD (0x2c)

#if defined(ACC_2G_RANGE)
#define ADXL362_ACCL_RANGE				(0x00)
#define ADXL362_ACCL_PER_LSB			((float)(0.001))

#elif defined(ACC_4G_RANGE)
#define ADXL362_ACCL_RANGE				(0x40)
#define ADXL362_ACCL_PER_LSB			((float)(0.002))

#elif defined(ACC_8G_RANGE)
#define ADXL362_ACCL_RANGE				(0x80)
#define ADXL362_ACCL_PER_LSB			((float)(0.004))

#endif

#if defined(ACC_BW_QUARTER)
#define ACC_BANDWIDTH (0x10)
#elif defined(ACC_BW_HALF)
#define ACC_BANDWIDTH (0x00)
#endif

#if defined(ACC_12R5HZ_ODR)
#define ADXL362_ACCL_ODR (0x00)
#elif defined(ACC_25HZ_ODR)
#define ADXL362_ACCL_ODR (0x01)
#elif defined(ACC_50HZ_ODR)
#define ADXL362_ACCL_ODR (0x02)
#elif defined(ACC_100HZ_ODR)
#define ADXL362_ACCL_ODR (0x03)
#elif defined(ACC_200HZ_ODR)
#define ADXL362_ACCL_ODR (0x04)
#elif defined(ACC_400HZ_ODR)
#define ADXL362_ACCL_ODR (0x05)
#endif



//-----Power Control register-----
#define ADXL362_POWER_CTL_REG_ADD (0x2d)

#if defined(ACC_NORMAL_NOISE)
#define ADXL362_LOW_NOISE (0x00)
#elif defined(ACC_LOW_NOISE)
#define ADXL362_LOW_NOISE (0x10)
#elif defined(ACC_ULTRALOW_NOISE)
#define ADXL362_LOW_NOISE (0x20)
#endif

#define ADXL362_MEASURE_MEASURE		(0x02)
#define ADXL362_MEASURE_STANDBY		(0x00)

#endif
