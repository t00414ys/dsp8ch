/*!******************************************************************************
 * @file    bme280_api.h
 * @brief   bme280 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BME280_API_H__
#define __BME280_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int bme280_init( unsigned int param );
void bme280_ctrl( int f_ena );
unsigned int bme280_rcv( unsigned int tick );
int bme280_conv( frizz_fp* humid_data );

int bme280_setparam( void *ptr );
unsigned int bme280_get_ver( void );
unsigned int bme280_get_name( void );

int bme280_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif	// __BME280_API_H__
