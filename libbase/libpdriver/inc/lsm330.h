/*!******************************************************************************
 * @file    stlsm330.h
 * @brief   stlsm330 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __LSM330_H__
#define __LSM330_H__
#include "lsm330_api.h"







/*
 *
 *		The rest of this is okay not change
 *
 */

//********************************************************
// ST LSM330 hardware setting
//********************************************************
// AFS_SEL=1: +- 4g
#define LSM330_DEG2RAD					0.01745329251994329576923690768489
// 32768/4 (LSB/g) => 4/32768 (g/LSB)
#define LSM330_ACCEL_PER_LSB			((float)(4./32768.))
// FS_SEL=3: +- 500 (degree/s)
// 32768/500 LSB/(deg/s) => 500/32768 (deg/s)/LSB => deg2rad*500/32768 (rad/s)/LSB
#define LSM330_GYRO_PER_LSB				((float)(LSM330_DEG2RAD*500./32768.))
// x (LSB/C) => 1/ x (C/LSB)
#define LSM330_TEMPERATURE_PER_LSB		1//((float)(1./128))

//********************************************************
// ST LSM330 hardware setting
//********************************************************
// Definition of pin:SPI_SDO
#if defined(USE_OPPO_BOARD)
#define	LSM330_ACC_addr							0x1E	// SAD[0] = low(pin:SDO)		// OPPO
#define	LSM330_GYRO_addr						0x6A	// SAD[0] = low(pin:SDO)		// OPPO

#elif defined(USE_LSM330_ayame_setting)
#define	LSM330_ACC_addr							0x1D	// SAD[0] = high(pin:SDO)		// this for ayame, jumper
#define	LSM330_GYRO_addr						0x6B	// SAD[0] = high(pin:SDO)		// this for ayame, jumper

#else
#define	LSM330_ACC_addr							0x1E	// SAD[0] = low(pin:SDO)		// OPPO
#define	LSM330_GYRO_addr						0x6A	// SAD[0] = low(pin:SDO)		// OPPO

#endif
//********************************************************


//******** register map  G sensor
#define	LSM330_WHO_AM_I_A						0x0F
#define	LSM330_WHO_AM_I_ID_A					0x40
#define	LSM330_CTRL_REG4_A						0x23
#define	LSM330_CTRL_REG5_A						0x20
#define	LSM330_CTRL_REG6_A						0x24
#define	LSM330_CTRL_REG7_A						0x25
#define	LSM330_STATUS_REG_A						0x27
#define	LSM330_OFF_X							0x10
#define	LSM330_OFF_Y							0x11
#define	LSM330_OFF_Z							0x12
#define	LSM330_CS_X								0x13
#define	LSM330_CS_Y								0x14
#define	LSM330_CS_Z								0x15
#define	LSM330_CL_L								0x16
#define	LSM330_CL_H								0x17
#define	LSM330_STAT								0x18
#define	LSM330_VFC_1							0x1B
#define	LSM330_VFC_2							0x1C
#define	LSM330_VFC_3							0x1D
#define	LSM330_VFC_4							0x1E
#define	LSM330_THRS3							0x1F
#define	LSM330_OUT_X_L_A						0x28
#define	LSM330_OUT_X_H_A						0x29
#define	LSM330_OUT_Y_L_A						0x2A
#define	LSM330_OUT_Y_H_A						0x2B
#define	LSM330_OUT_Z_L_A						0x2C
#define	LSM330_OUT_Z_H_A						0x2D
#define	LSM330_FIFO_CTRL_REG_A					0x2E
#define	LSM330_FIFO_SRC_REG_A					0x2F
#define	LSM330_CTRL_REG2_A						0x21




//******** register map  Gyro
#define	LSM330_WHO_AM_I_G						0x0F
#define	LSM330_WHO_AM_I_ID_G					0xD4
#define	LSM330_CTRL_REG1_G						0x20
#define	LSM330_CTRL_REG2_G						0x21
#define	LSM330_CTRL_REG3_G						0x22
#define	LSM330_CTRL_REG4_G						0x23
#define	LSM330_CTRL_REG5_G						0x24
#define	LSM330_REFERENCE_G						0x25
#define	LSM330_OUT_TEMP_G						0x26
#define	LSM330_STATUS_REG_G						0x27
#define	LSM330_OUT_X_L_G						0x28
#define	LSM330_OUT_X_H_G						0x29
#define	LSM330_OUT_Y_L_G						0x2A
#define	LSM330_OUT_Y_H_G						0x2B
#define	LSM330_OUT_Z_L_G						0x2C
#define	LSM330_OUT_Z_H_G						0x2D
#define	LSM330_FIFO_CTRL_REG_G					0x2E
#define	LSM330_FIFO_SRC_REG_G					0x2F

//******** register value G sensor
#define	acc_power_3p125Hz						0x10
#define	acc_power_6p25Hz						0x20
#define	acc_power_12p5Hz						0x30
#define	acc_power_25Hz							0x40
#define	acc_power_50Hz							0x50
#define	acc_power_100Hz							0x60
#define	acc_power_400Hz							0x70
#define	acc_power_800Hz							0x80
#define	acc_power_1600Hz						0x90
#define	acc_power_MASK							0xF0
#define acc_axis_Enable							0x07
#define	acc_power_down							0x00
#define	acc_BDU_on								0x08
#define	acc_BDU_off								0x00
#define acc_bandwidth_50Hz						0xC0
#define acc_bandwidth_200Hz						0x40
#define acc_bandwidth_400Hz						0x80
#define acc_bandwidth_800Hz						0x00
#define acc_2G									0x00
#define acc_4G									0x08
#define acc_6G									0x10
#define acc_8G									0x18
#define acc_16G									0x20

//******** register value Gyro
#define gyro_ODR190Hz_12p5Hz					0x40
#define gyro_ODR190Hz_25Hz						0x50
#define gyro_ODR190Hz_50Hz						0x60
#define gyro_ODR190Hz_70Hz						0x70
#define gyro_normalmode							0x08
#define gyro_sleepmode							0x08
#define gyro_axis_Enable						0x07
#define gyro_powerdown							0x00
#define gyro_power_MASK							0x0F
#define gyro_FS_250dps							0x00
#define gyro_FS_500dps							0x10
#define gyro_FS_2000dps							0x20
#define	gyro_BDU_on								0x80
#define	gyro_BDU_off							0x00
#define	gyro_HPF_on								0x10
#define	gyro_HPF_off							0x00
#define	gyro_Out_Sel_0							0x00
#define	gyro_Out_Sel_1							0x01
#define	gyro_Out_Sel_2							0x02
#define	gyro_Out_Sel_3							0x03
#define	gyro_HPF_mode0							0x00
#define	gyro_HPF_mode1							0x10
#define	gyro_HPF_mode2							0x20
#define	gyro_HPF_mode3							0x30
#define	gyro_HPF_HPCF0							0x00
#define	gyro_HPF_HPCF1							0x01
#define	gyro_HPF_HPCF2							0x02
#define	gyro_HPF_HPCF3							0x03
#define	gyro_HPF_HPCF4							0x04
#define	gyro_HPF_HPCF5							0x05
#define	gyro_HPF_HPCF6							0x06
#define	gyro_HPF_HPCF7							0x07
#define	gyro_HPF_HPCF8							0x08
#define	gyro_HPF_HPCF9							0x09

#endif /*  */
