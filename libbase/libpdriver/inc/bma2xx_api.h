/*!******************************************************************************
 * @file    bma2xx_api.h
 * @brief   bma2xx sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMA2XX_API_H__
#define __BMA2XX_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int bma2xx_init( unsigned int param );
void bma2xx_ctrl_accl( int f_ena );
unsigned int bma2xx_rcv_accl( unsigned int tick );
int bma2xx_conv_accl( frizz_fp data[3] );

int bma2xx_accl_get_condition( void *data );
int bma2xx_accl_get_raw_data( void *data );

int bma2xx_setparam_accl( void *buffer );
unsigned int bma2xx_get_ver( void );
unsigned int bma2xx_get_name( void );

#ifdef __cplusplus
}
#endif

#endif	//#ifndef __BMA2XX_API_H__
