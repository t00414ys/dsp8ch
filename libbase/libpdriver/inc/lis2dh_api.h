/*!******************************************************************************
 * @file    lis2dh_api.h
 * @brief   lis2dh sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LIS2DH_API_H__
#define __LIS2DH_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lis2dh_init( unsigned int param );
void lis2dh_ctrl_accl( int f_ena );
unsigned int lis2dh_rcv_accl( unsigned int tick );
int lis2dh_conv_accl( frizz_fp *data );
int lis2dh_setparam_accl( void *ptr );
unsigned int lis2dh_get_ver( void );
unsigned int lis2dh_get_name( void );

int lis2dh_accl_get_condition( void *data );
int lis2dh_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif

#endif
