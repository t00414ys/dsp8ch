/*!******************************************************************************
 * @file    hmc5883_api.h
 * @brief   hmc5883 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_API_H__
#define __ADXL362_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int hmc5883_init( unsigned int param );
void hmc5883_ctrl_magn( int f_ena );
unsigned int hmc5883_rcv_magn( unsigned int tick );
int hmc5883_conv_magn( frizz_fp data[3] );

int hmc5883_setparam_magn( void *ptr );
unsigned int hmc5883_get_ver( void );
unsigned int hmc5883_get_name( void );

int hmc5883_get_condition( void *data );
int hmc5883_get_raw_data( void *data );

int hmc5883_magn_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif

