/*!******************************************************************************
 * @file    ads8332.h
 * @brief   ads8332 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          ADXL362 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_H__
#define __ADXL362_H__
#include "hmc5883_api.h"

/*
 *
 *		By environment, you must edit this section.
 *
 */



#endif
