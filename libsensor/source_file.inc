#######
#
#  libvsensor files
#

LIBRARIES	= $(LIBDIR)/libsensor.a

##
#
##
ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
CFLAGS_LIB	+= -mrename-section-.text=.iram0.text
endif

##
#
##
SRCS	=
SRCS	+=	libsensors/src/accl/accl_hpf.c
SRCS	+=	libsensors/src/accl/accl_line.c
SRCS	+=	libsensors/src/accl/accl_lpf.c
SRCS	+=	libsensors/src/accl/accl_pos_det.c
SRCS	+=	libsensors/src/accl/accl_pow.c
SRCS	+=	libsensors/src/accl/accl_stat.c
#SRCS	+=	libsensors/src/activity/activity_detector.c
#SRCS	+=	libsensors/src/activity/calorie_sen.c
#SRCS	+=	libsensors/src/activity/motion_detector.c
#SRCS	+=	libsensors/src/activity/motion_sensing.c
SRCS	+=	libsensors/src/gyro/gyro_hpf.c
SRCS	+=	libsensors/src/gyro/gyro_lpf.c
SRCS	+=	libsensors/src/magn/mag_hard.c
SRCS	+=	libsensors/src/magn/mag_soft.c
SRCS	+=	libsensors/src/magn/mag_util.c
SRCS	+=	libsensors/src/magn/magn_calib_hard.c
SRCS	+=	libsensors/src/magn/magn_calib_raw.c
SRCS	+=	libsensors/src/magn/magn_calib_soft.c
SRCS	+=	libsensors/src/magn/magn_lpf.c
SRCS	+=	libsensors/src/magn/magn_param.c
SRCS	+=	libsensors/src/magn/magn_uncalib.c
SRCS	+=	libsensors/src/rotation/gravity.c
SRCS	+=	libsensors/src/rotation/orientation.c
SRCS	+=	libsensors/src/rotation/rot_grav_vec.c
SRCS	+=	libsensors/src/rotation/rot_lpf_vec.c
SRCS	+=	libsensors/src/rotation/rot_vec.c
SRCS	+=	libsensors/src/util/cyclic_sensor.c
SRCS	+=	libpsensor/src/base_driver.c
SRCS	+=	libpsensor/src/accl_raw.c
SRCS	+=	libpsensor/src/adc_raw.c
SRCS	+=	libpsensor/src/ecg_raw.c
SRCS	+=	libpsensor/src/gyro_raw.c
SRCS	+=	libpsensor/src/humidity_raw.c
SRCS	+=	libpsensor/src/light_raw.c
SRCS	+=	libpsensor/src/magn_raw.c
SRCS	+=	libpsensor/src/oeld_raw.c
SRCS	+=	libpsensor/src/ppg_raw.c
SRCS	+=	libpsensor/src/pres_raw.c
SRCS	+=	libpsensor/src/prox_raw.c
SRCS	+=	libpsensor/src/spo2_raw.c

##
#
##
INCS	=
ifneq ($(ENVIRONMENT),RUN_ON_FRIZZ)
INCS	+=	-I$(WORKSPACE_LOC)/$(STUBDIR)/include
endif
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libfrizz/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libhub/inc
INCS	+=	-I$(WORKSPACE_LOC)/libbase/frizz_driver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc/config
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/drivers
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libpsensor/inc/config
INCS	+=	-I$(WORKSPACE_LOC)/libND/libpdriver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libsensor/libsensors/inc/driver
INCS	+=	-I$(WORKSPACE_LOC)/libbase/libpdriver/inc
INCS	+=	-I$(WORKSPACE_LOC)/libcommon/inc
INCS	+=	-I$(WORKSPACE_LOC)/target/libcommon/inc
