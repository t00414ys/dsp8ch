/*!******************************************************************************
 * @file    magn_calib_soft.h
 * @brief   virtual magnet calibration soft sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup MAGNET_CALIB_SOFT MAGNET CALIB SOFT
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief magnet calibration soft sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out Soft-Iron calibration + Soft-Iron magnetic field parameter.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_MAGNET_CALIB_SOFT<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_MAGNET_RAW, #SENSOR_ID_MAGNET_PARAMETER<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_MAGNET_CALIB_SOFT" color=red];
 *		struct2 [label="SENSOR_ID_MAGNET_RAW" color=blue];
 *		struct3 [label="SENSOR_ID_MAGNET_PARAMETER" color=blue];
 *		struct2 -> struct1; struct3 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)<br>
 */

#ifndef __MAGN_CALIB_SOFT_H__
#define __MAGN_CALIB_SOFT_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup MAGNET_CALIB_SOFT MAGNET CALIB SOFT
 *  @{
 */

/**
 *@brief	Initialize Soft-Iron magnet calibration sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* magn_calib_soft_init( void );
/** @} */
#endif
