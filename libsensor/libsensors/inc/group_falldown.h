/*!******************************************************************************
 * @file    group_falldown.h
 * @brief   group of virtual falldown sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief falldown header group
 */
#ifndef __GROUP_FALLDOWN_H__
#define __GROUP_FALLDOWN_H__

#include "accl_fall_down.h"
#include "accl_pos_det.h"

#endif
