/*!******************************************************************************
 * @file    magn_uncalib.h
 * @brief   virtual magnet uncalibration sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup MAGNET_UNCALIB MAGNET UNCALIB
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief magnet uncalibration sensor
 *  @brief -# <B>Contents</B><br>
 *  @brief This sensor for taking out a parameter of SoftIron and offset between SoftIron and HardIron.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_MAGNET_UNCALIB<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_MAGNET_CALIB_SOFT, #SENSOR_ID_MAGNET_CALIB_HARD<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_MAGNET_UNCALIB" color=red];
 *		struct2 [label="SENSOR_ID_MAGNET_CALIB_SOFT" color=blue];
 *		struct3 [label="SENSOR_ID_MAGNET_CALIB_HARD" color=blue];
 *		struct2 -> struct1; struct3 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __MAGN_UNCALIB_H__
#define __MAGN_UNCALIB_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup MAGNET_UNCALIB MAGNET UNCALIB
 *  @{
 */

/**
 *@brief	Initialize magnet uncalibration sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* magn_uncalib_init( void );
/** @} */
#endif
