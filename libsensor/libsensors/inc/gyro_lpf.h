/*!******************************************************************************
 * @file    gyro_lpf.h
 * @brief   virtual gyro lpf sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup GYRO_LPF GYRO LPF
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief gyro lpf sensor
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out a low frequency component of a gyro sensor.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_GYRO_LPF<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_GYRO_RAW, #SENSOR_ID_ACCEL_RAW<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_GYRO_LPF" color=red];
 *		struct2 [label="SENSOR_ID_GYRO_RAW" color=blue];
 *		struct3 [label="SENSOR_ID_ACCEL_RAW" color=blue];
 *		struct2 -> struct1; struct3 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 * Detection Timing is every 5 minutes.
 * But, if sensor isn't stationary, Detection Timing is longer than 5 minutes.
 */
#ifndef __GYRO_LPF_H__
#define __GYRO_LPF_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup GYRO_LPF GYRO LPF
 *  @{
 */

/**
 *@brief	Initialize gyro lpf sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* gyro_lpf_init( void );

/** @} */
#endif
