/*!******************************************************************************
 * @file    mag_soft.h
 * @brief   utility header for Soft-Iron Comp
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAG_SOFT_H__
#define __MAG_SOFT_H__

#include "frizz_type.h"
#include "matrix.h"

typedef struct {
	// for elimination of Soft-Iron
	Matrix		*xx_m;				// 9 x 9: summed x x^t
	Matrix		*x_v;				// 9 x 1: summed x
	Matrix		*a_v;				// 9 x 1: a

	Matrix		*A_m;				// 3 x 3: A

	Matrix		*Inv9x9;			// 9 x 9:
	Matrix		*Inv3x3;			// 3 x 3:

	Matrix		*T0_9x1;			// 9 x 1: Temp
	Matrix		*T0_9x9;			// 9 x 9: Temp
	Matrix		*T0_3x1;			// 3 x 1: Temp
	Matrix		*T0_3x3;			// 3 x 3: Temp

	Matrix		*eig_vec;			// 3 x 3: eigenvector
	Matrix		*eig_val;			// 3 x 3: eigenvalue
} SoftIron_t;

typedef SoftIron_t* SoftIron_h;

/**
 * @brief create SoftIron
 *
 * @return object
 */
SoftIron_h SoftIron_New( void );

/**
 * @brief destroy SoftIron
 *
 * @param [in]	h SoftIron Handle
 */
void SoftIron_Delete( SoftIron_h h );

/**
 * @brief initialize SoftIron
 *
 * @param [in]	h SoftIron Handle
 */
void SoftIron_Init( SoftIron_h h );

/**
 * @brief input data
 *
 * @param [in]	h SoftIron Handle
 * @param [in]	mag magnet sensor data
 */
void SoftIron_PushData( SoftIron_h h, frizz_fp mag[3] );

/**
 * @brief calibration calculation
 *
 * @param [in]	h SoftIron Handle
 * @param [out]	hard Hard-Iron Matrix: 3 x 1
 * @param [out]	soft Soft-Iron Matrix: 3 x 3
 *
 * @return -1:can't calculate, 0:success
 */
int SoftIron_CalibCalc( SoftIron_h h, Matrix *hard, Matrix *soft );

#endif//__MAG_SOFT_H__
