/*!******************************************************************************
 * @file    rot_lpf_vec_if.h
 * @brief   virtual rotation lpf vector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ROT_LPF_VEC_IF_H__
#define __ROT_LPF_VEC_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ROTATION_LPF_VECTOR ROTATION LPF VECTOR
 *  @{
 */
#define ROTATION_LPF_VECTOR_ID	SENSOR_ID_ROTATION_LPF_VECTOR  //!< rotation lpf vector sensor interface interface ID

/**
 * @struct rot_lpf_vec_data_t
 * @brief Output data structure for rotation vector
 */
typedef struct {
	FLOAT		data[4];	//!< quaternion 0:cos(theta/2), 1~3: uvec * sin(theta/2)
	FLOAT		accr;		//!< accuracy [rad]
} rot_lpf_vec_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
