/*!******************************************************************************
 * @file    bpl_sen_if.h
 * @brief   virtual blood pressure learn sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BPL_SEN_IF_H__
#define __BPL_SEN_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SENSOR_ID_BLOOD_PRESSURE_LEARN BLOOD PRESSURE LEARN
 *  @{
 */
#define BLOOD_PRESSURE_LEARN_ID	SENSOR_ID_BLOOD_PRESSURE_LEARN //!< Pedestrian Dead Reckoning

/**
 * @struct ST_BLOOD_PRESSURE_LEARNING
 * @brief Output data structure for blood pressure learn sensor
 * @brief And #BLOOD_PRESSURE_CMD_BPLEARN_PARAM command param
 */
typedef struct {
	//
	unsigned short  version;            //!< structure version
	unsigned short  length;             //!< structure length
	short           sab[3];             //!< learning data
	short           dab[3];             //!< learning data
	short           st[24];             //!< learning source data
	short           dt[24];             //!< learning source data
	short           ps[24];             //!< learning source data
	short           sbp_max;            //!< learn deta sbp max
	short           sbp_min;            //!< learn deta sbp min
	short           dbp_max;            //!< learn deta dbp max
	short           dbp_min;            //!< learn deta dbp min
	short           lp;                 //!< learning source data pointer
	short           reserve;            //!< reserve
	unsigned int    mark;               //!< end marker (0x12345678)
} ST_BLOOD_PRESSURE_LEARNING;

/**
 * @struct ST_BLOOD_PRESSURE_SETTING
 * @brief #BLOOD_PRESSURE_CMD_BPSET_PARAM command param
 */
typedef struct {
	unsigned short  version;            //!< structure version
	unsigned short  length;             //!< length
	char            name[8];            //!< Name
	short           age;                //!< Age
	short           weight;             //!< Weught
	short           gender;             //!< Gender 0:Male 1:Female
	short           BSBP[2];            //!< BSBP[0] = Usual max BP : BSBP[1] = Usual min BP
	unsigned char   debug;              //!< Debug  0:NOT debug mode 1:debug mode(Use Parameter)
	unsigned char   sp;                 //!< special parameter
	short           hr_param1;          //!< param 1
	short           hr_param2;          //!< param 2
	short           hr_param3;          //!< param 3
	short           hr_param4;          //!< param 4
	short           hr_param5;          //!< param 5
	short           hr_param6;          //!< param 6
	short           hr_param7;          //!< param 7
	short           hr_param8;          //!< param 8
	unsigned int    mark;               //!< end marker (0x12345678)
} ST_BLOOD_PRESSURE_SETTING;

/** @} */

#define		BLOOD_PRESSURE_CMD_VERSION			(0x0004)		//!<version

#define		BLOOD_PRESSURE_CMD_MARKER_CODE		(0x12345678)	//!<marker code
#define		BLOOD_PRESSURE_CMD_MARKER_REV		(0x56781234)	//!<marker revision
#define		BLOOD_PRESSURE_CMD_MARKER_REV2		(0x34127856)	//!<marker revision2
#define		BLOOD_PRESSURE_CMD_MARKER_HOST		(0x78563412)	//!<marker host

/** @defgroup SENSOR_ID_BLOOD_PRESSURE_LEARN BLOOD PRESSURE LEARN
 *  @{
 */

/**
 * @name Command List
 */
//@{

/**
 * @brief Set Setting (personal data) Param
 * @param cmd_code BLOOD_PRESSURE_CMD_BPSET_PARAM
 * @param param following structure.
 * @brief parameter structure is #ST_BLOOD_PRESSURE_SETTING.
 * @return  0: OK, -1: Command Error
 */
#define BLOOD_PRESSURE_CMD_BPSET_PARAM			0
/**
 * @brief Set Learning Param
 * @param cmd_code BLOOD_PRESSURE_CMD_BPLEARN_PARAM
 * @param param following structure.
 * @brief paremeter structure is #ST_BLOOD_PRESSURE_LEARNING.
 * @return  0: OK, -1: Command Error
 */
#define BLOOD_PRESSURE_CMD_BPLEARN_PARAM		1
/**
 * @brief Get Version
 * @param cmd_code BLOOD_PRESSURE_GET_VERSION
 * @return BLOOD_PRESSURE version, -1: Command Error
 */
#define BLOOD_PRESSURE_GET_VERSION				0xFF

//@}

/** @} */
#endif
