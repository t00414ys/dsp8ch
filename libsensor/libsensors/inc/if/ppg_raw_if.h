/*!******************************************************************************
 * @file    ppg_raw_if.h
 * @brief   virtual ppg raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup PPG_RAW PPG RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief PPG raw sensor<BR>
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an ppg component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_PPG_RAW <BR>
 *  Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : Continuous (This sensor type output sensor
 *			data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __PPG_RAW_IF_H__
#define __PPG_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup PPG_RAW PPG RAW
 *  @{
 */

#define PPG_RAW_ID	SENSOR_ID_PPG_RAW //!< PPG from Physical Sensor interface ID

/**
 * @struct ppg_raw_data_t
 * @brief Output data structure for a PPG sensor
 */
typedef struct {
	unsigned int	data;		//!<  pulse data
} ppg_raw_data_t;

/** @} */

/**
 * @name Command List
 */
//@{
/**
 * @brief Get Threshold
 * @param cmd_code SENSOR_GET_THRESHOLD
 * @return 0: Command Ok, -1: Command Error
 */
#define SENSOR_GET_THRESHOLD			0x00
/**
 * @brief Get Threshold
 * @param cmd_code SENSOR_PPG_GET_REAL_RAW_DATA
 * @return 0: Command Ok, -1: Command Error
 */
#define SENSOR_PPG_GET_REAL_RAW_DATA	0x01
//@}

#endif
