/*!******************************************************************************
 * @file    accl_stat_if.h
 * @brief   virtual accel statistics if interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_STAT_IF_H__
#define __ACCL_STAT_IF_H__

#include "libsensors_id.h"

/** @defgroup ACCEL_STATISTICS ACCEL STATISTICS
 *  @{
 */
#define ACCEL_STAT_ID	SENSOR_ID_ACCEL_STATISTICS	//!< An accel statistics interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
