/*!******************************************************************************
 * @file    cyclic_sensor_if.h
 * @brief   virtual cyclic timer sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __CYCLIC_SENSOR_IF_H__
#define __CYCLIC_SENSOR_IF_H__

#include "libsensors_id.h"

#define CYCLIC_TIMER_ID	SENSOR_ID_CYCLIC_TIMER	//!< A cyclic timer sensor interface ID

/**
 * @struct cyclic_timer_data_t
 * @brief Cycle sensor output data structure
 */
typedef struct {
	unsigned int		count;	//!< count value
} cyclic_timer_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief Clear counter
 * @param cmd_code CYCLIC_TIMER_CMD_CLEAR_COUNT
 * @return 0: OK other:NG
 */
#define CYCLIC_TIMER_CMD_CLEAR_COUNT		0x00

//@}
#endif
