/*!******************************************************************************
 * @file    motion_sensing_if.h
 * @brief   virtual motion sensing sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MOTION_SENSING_IF_H__
#define __MOTION_SENSING_IF_H__

#include "libsensors_id.h"

/** @defgroup MOTION_SENSING MOTION SENSING
 *  @{
 */
#define MOTION_SENSING_ID SENSOR_ID_MOTION_SENSING	//!< An motion sensing interface ID

/**	@struct motion_sensing_data_t
 *	@brief Output data structure for motion detect sensor
 */
typedef struct {
	unsigned int activity;			//!< motion status 0x00:STOP 0x01:REST 0x02:WALK 0x03:RUN
} motion_sensing_data_t;

/**
 * @name Command List
 */
//@{

/**
 * @brief	set period of stop state detected (unit: minute)
 * @param	cmd_code	MOTION_SENSING_CMD_SET_STOP_PERIOD
 * @param param[0] unsigned int  stop period
 * @return	0:OK other:NG
 */
#define MOTION_SENSING_CMD_SET_STOP_PERIOD				0x00

/**
 * @brief Set enable/disable report of motion status
 *
 * @param cmd_code MOTION_SENSING_CMD_REPORT_ENABLE
 * @param param[0] unsigned int  report status\n
 *  parameter description:\n
 * Bit0 : stop status\n
 * Bit1 : rest status\n
 * Bit2 : walk status\n
 * Bit3 : run status\n
 * If "stop status" is only enabled to report, parameter is 0b00000001 = 0x01.\n
 * if all status are enabled to report, parameter is 0b00001111 = 0x0F.
 * @return 0: OK, -1: Command Error
 */
#define	MOTION_SENSING_CMD_REPORT_ENABLE				0x01

//@}

/** @} */
#endif
