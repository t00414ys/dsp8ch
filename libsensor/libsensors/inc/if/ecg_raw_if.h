/*!******************************************************************************
 * @file    ecg_raw_if.h
 * @brief   virtual ecg raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup PPG_RAW PPG RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief ecg raw sensor<BR>
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an ecg component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_PPG_RAW <BR>
 *  Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : Continuous (This sensor type output sensor
 *			data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __PPG_RAW_IF_H__
#define __PPG_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup PPG_RAW PPG RAW
 *  @{
 */

#define ECG_RAW_ID	SENSOR_ID_ECG_RAW //!< PPG from Physical Sensor interface ID

/**
 * @struct ecg_raw_data_t
 * @brief Output data structure for an accel sensor
 */
typedef struct {
	unsigned short	data;		//!< data:ecg data
	unsigned short  changed;	//!< data:changed
} ecg_raw_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
