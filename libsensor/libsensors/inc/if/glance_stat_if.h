/*!******************************************************************************
 * @file    glance_stat_if.h
 * @brief   virtual glance status if sensor interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GLANCE_STAT_IF_H__
#define __GLANCE_STAT_IF_H__

#include "libsensors_id.h"

/** @defgroup GLANCE_STAT GLANCE STATUS
 *  @{
 */
#define GLANCE_STAT_ID SENSOR_ID_GLANCE_STATUS	//!< An glance status sensor interface ID

/**
 * @struct glance_stat_e
 * @brief Output data structure for gravity direction sensor
 */
typedef enum {
	GLANCE_STAT_INIT = -1,		//!< status of initial
	GLANCE_STAT_UNWATCHING,		//!< status of not watching display
	GLANCE_STAT_WATCHING,		//!< status of watching display
} glance_stat_e;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
