/*!******************************************************************************
 * @file    spo2_raw_if.h
 * @brief   virtual spo2 raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup SPO2_RAW SPO2 RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief SPO2 raw sensor<BR>
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an spo2 component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_SPO2_RAW <BR>
 *  Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : Continuous (This sensor type output sensor
 *			data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __SPO2_RAW_IF_H__
#define __SPO2_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SPO2_RAW SPO2 RAW
 *  @{
 */

#define SPO2_RAW_ID	SENSOR_ID_SPO2_RAW //!< SPO2 from Physical Sensor interface ID

/**
 * @struct spo2_raw_data_t
 * @brief Output data structure for an accel sensor
 */
typedef struct {
	unsigned int	data;           //!<  Red Led data
	unsigned int	data_sub;       //!<  IR Led data
} spo2_raw_data_t;

/** @} */

/**
 * @name Command List
 */
//@{
/**
 * @brief Get Threshold
 * @param cmd_code SENSOR_GET_THRESHOLD
 * @return 0: Command Ok, -1: Command Error
 */
#define SENSOR_GET_THRESHOLD			0x00
/**
 * @brief Get Threshold
 * @param cmd_code SENSOR_SPO2_GET_REAL_RAW_DATA
 * @return 0: Command Ok, -1: Command Error
 */
#define SENSOR_SPO2_GET_REAL_RAW_DATA	0x01
//@}

#endif
