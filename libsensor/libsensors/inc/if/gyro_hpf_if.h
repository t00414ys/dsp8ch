/*!******************************************************************************
 * @file    gyro_hpf_if.h
 * @brief   virtual gyro hpf sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GYRO_HPF_IF_H__
#define __GYRO_HPF_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif


/** @defgroup GYRO_HPF GYRO HPF
 *  @{
 */
#define GYRO_HPF_ID	SENSOR_ID_GYRO_HPF	//!< A gyro hpf sensor interface ID

/**
 * @struct gyro_hpf_data_t
 * @brief Output data structure for gyro hpf sensor
 */
typedef struct {
	FLOAT		data[3];	//!< Angular velocity vector [rad/s] data[0]:X-axis Component[rad/s], data[1]:Y-axis Component[rad/s], data[2]:Z-axis Component[rad/s]
} gyro_hpf_data_t;

/**
 * @name Command List
 *
 * @note none
 */
//@{
//@}

/** @} */
#endif
