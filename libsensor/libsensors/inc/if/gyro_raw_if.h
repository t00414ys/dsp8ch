/*!******************************************************************************
 * @file    gyro_raw_if.h
 * @brief   virtual gyro raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup GYRO_RAW GYRO RAW
 * @ingroup physicalgroup Physical Sensor
 *  @brief gyro raw sensor
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an angular velocity vector and degreeC components.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_GYRO_RAW <BR>
 *  @brief Parent Sensor ID : none <BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __GYRO_RAW_IF_H__
#define __GYRO_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup GYRO_RAW GYRO RAW
 *  @{
 */

#define GYRO_RAW_ID	SENSOR_ID_GYRO_RAW	//!< gyro raw sensor interface ID

/**
 * @struct gyro_raw_data_t
 * @brief Output data structure for gyro sensor
 */
typedef struct {
	FLOAT		w[3];	//!< Angular velocity vector [rad/s] data[0]:X-axis Component[rad/s], data[1]:Y-axis Component[rad/s], data[2]:Z-axis Component[rad/s]
	FLOAT		c;		//!< degreeC
} gyro_raw_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
