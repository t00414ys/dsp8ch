/*!******************************************************************************
 * @file    mag_soft.c
 * @brief   utility code for Soft-Iron Comp
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "frizz_mem.h"
#include "frizz_const.h"
#include "matrix.h"
#include "frizz_type.h"
#include "frizz_math.h"
#include "mag_soft.h"

//#define PLOT_LOCUS

static void mtx_set_col_vec( Matrix *mtx, int row, int col, frizz_fp4w *vec )
{
	frizz_fp *data = ( frizz_fp* )vec;
	mtx->data[row + 0][col] = data[0];
	mtx->data[row + 1][col] = data[1];
	mtx->data[row + 2][col] = data[2];
}

/**
 * @brief create SoftIron
 *
 * @return object
 */
SoftIron_h SoftIron_New( void )
{
	SoftIron_t *o = ( SoftIron_t* )frizz_malloc( sizeof( SoftIron_t ) );
	if( o != NULL ) {
		memset(o,0,sizeof(SoftIron_t));

		// Matrix
		o->xx_m = alloc_matrix( 9, 9 );
		o->x_v = alloc_matrix( 9, 1 );
		o->a_v = alloc_matrix( 9, 1 );

		o->A_m = alloc_matrix( 3, 3 );

		o->Inv9x9 = alloc_matrix( 9, 9 );
		o->Inv3x3 = alloc_matrix( 3, 3 );

		o->T0_9x1 = alloc_matrix( 9, 1 );
		o->T0_9x9 = alloc_matrix( 9, 9 );
		o->T0_3x1 = alloc_matrix( 3, 1 );
		o->T0_3x3 = alloc_matrix( 3, 3 );

		o->eig_vec = alloc_matrix( 3, 3 );
		o->eig_val = alloc_matrix( 3, 3 );

		SoftIron_Init( o );
	}

	return o;
}

/**
 * @brief destroy SoftIron
 *
 * @param [in]	h SoftIron Handle
 */
void SoftIron_Delete( SoftIron_h h )
{
	SoftIron_t* o = ( SoftIron_t* )h;

	// Matrix
	free_matrix( o->xx_m );
	free_matrix( o->x_v );
	free_matrix( o->a_v );

	free_matrix( o->A_m );

	free_matrix( o->Inv9x9 );
	free_matrix( o->Inv3x3 );

	free_matrix( o->T0_9x1 );
	free_matrix( o->T0_9x9 );
	free_matrix( o->T0_3x1 );
	free_matrix( o->T0_3x3 );

	free_matrix( o->eig_vec );
	free_matrix( o->eig_val );

	frizz_free( o );
}

/**
 * @brief initialize SoftIron
 *
 * @param [in]	h SoftIron Handle
 */
void SoftIron_Init( SoftIron_h h )
{
	SoftIron_t* o = ( SoftIron_t* )h;
	scale_matrix( o->xx_m, FRIZZ_CONST_ZERO );
	scale_matrix( o->x_v, FRIZZ_CONST_ZERO );
}

/**
 * @brief input data
 *
 * @param [in]	h SoftIron Handle
 * @param [in]	mag magnet sensor data
 */
void SoftIron_PushData( SoftIron_h h, frizz_fp mag[3] )
{
	SoftIron_t* o = ( SoftIron_t* )h;
	frizz_fp4w vec, sqr, crs, dbl;
	frizz_fp *p0, *p1;
	frizz_fp x;
	// x y z
	p0 = ( frizz_fp* )&vec;
	p0[0] = mag[0];
	p0[1] = mag[1];
	p0[2] = mag[2];
	p0[3] = FRIZZ_CONST_ZERO;
	// y z x
	p1 = ( frizz_fp* )&crs;
	p1[0] = p0[1];
	p1[1] = p0[2];
	p1[2] = p0[0];
	p1[3] = FRIZZ_CONST_ZERO;
	// x^2, y^2, z^2
	sqr = vec * vec;
	// 2x 2y 2z
	dbl = FRIZZ_CONST_TWO * vec;
	// 2xy, 2yz, 2zx
	crs = dbl * crs;
	// summation
	p0 = ( frizz_fp* )&sqr;
	x = p0[0];
	p0[0] = FRIZZ_CONST_ONE;
	mtx_set_col_vec( o->T0_9x1, 0, 0, &sqr );
	mtx_set_col_vec( o->T0_9x1, 3, 0, &crs );
	mtx_set_col_vec( o->T0_9x1, 6, 0, &dbl );
	mul_trmatrix( o->T0_9x1, o->T0_9x1, o->T0_9x9 );
	add_matrix( o->xx_m, o->T0_9x9, o->xx_m );
	scale_matrix( o->T0_9x1, x );
	add_matrix( o->x_v, o->T0_9x1, o->x_v );
}

/**
 * @brief calibration calculation
 *
 * @param [in]	h SoftIron Handle
 * @param [out]	hard Hard-Iron Matrix: 3 x 1
 * @param [out]	soft Soft-Iron Matrix: 3 x 3
 *
 * @return -1:can't calculate, 0:success
 */
int SoftIron_CalibCalc( SoftIron_h h, Matrix *hard, Matrix *soft )
{
	SoftIron_t* o = ( SoftIron_t* )h;
	// calculate
	if( invert_matrix( o->xx_m, o->Inv9x9, o->T0_9x9 ) == 0 ) {
		mul_matrix( o->Inv9x9, o->x_v, o->a_v );
		// create A
		{
			frizz_fp **A = o->A_m->data;
			frizz_fp **a = o->a_v->data;
			// A
			A[0][0] = FRIZZ_CONST_ONE;
			A[1][1] = -a[1][0];
			A[2][2] = -a[2][0];
			A[0][1] = A[1][0] = -a[3][0];
			A[1][2] = A[2][1] = -a[4][0];
			A[0][2] = A[2][0] = -a[5][0];
#ifdef PLOT_LOCUS
			print_matrix( o->A_m, "A_m" );
#endif
		}
		// hard iron
		if( invert_matrix( o->A_m, o->Inv3x3, o->T0_3x3 ) == 0 ) {
			frizz_fp **t = o->T0_3x1->data;
			frizz_fp **a = o->a_v->data;
			t[0][0] = a[6][0];
			t[1][0] = a[7][0];
			t[2][0] = a[8][0];
			mul_matrix( o->Inv3x3, o->T0_3x1, hard );
		}
		// get eigen vector
		eigen_matrix( o->A_m, o->eig_vec, o->eig_val );
		// calc W^(-1)
		if( invert_matrix( o->eig_vec, o->Inv3x3, o->T0_3x3 ) == 0 ) {
			frizz_fp amp;
			frizz_fp **e = o->eig_val->data;
			// fit to  minor princibal axis
			if( frizz_fabs( e[0][0] ) < frizz_fabs( e[1][1] ) ) {
				amp = e[1][1];
			} else {
				amp = e[0][0];
			}
			if( frizz_fabs( amp ) < frizz_fabs( e[2][2] ) ) {
				amp = e[2][2];
			}
			amp = frizz_div( FRIZZ_CONST_ONE, amp );
			scale_matrix( o->A_m, amp );
			scale_matrix( o->eig_val, amp );
			// Square Root for eigen value
			copy_matrix( o->eig_val, o->T0_3x3 );
			set_matrix_identity( o->eig_val );
			o->eig_val->data[0][0] = frizz_sqrt( frizz_fabs( o->T0_3x3->data[0][0] ) );
			o->eig_val->data[1][1] = frizz_sqrt( frizz_fabs( o->T0_3x3->data[1][1] ) );
			o->eig_val->data[2][2] = frizz_sqrt( frizz_fabs( o->T0_3x3->data[2][2] ) );
			mul_matrix( o->eig_vec, o->eig_val, o->T0_3x3 );
			mul_matrix( o->T0_3x3, o->Inv3x3, soft );
			// complete
#ifdef PLOT_LOCUS
			{
				frizz_fp **p_o, **p_e;
				p_o = hard->data;
				print_matrix( o->A_m, "A_m" );
				print_matrix( soft, "soft" );
				print_matrix( hard, "hard" );
				printf( "#amp: %e\n", amp );
				printf( "na na na na na na na na na " );
				printf( "%e %e %e ", p_o[0][0], p_o[1][0], p_o[2][0] );
				printf( "%e %e %e ", p_o[0][0], p_o[1][0], p_o[2][0] );
				printf( "%e %e %e ", p_o[0][0], p_o[1][0], p_o[2][0] );
				printf( "na na na " );
				printf( "\n" );
				p_e = o->eig_vec->data;
				printf( "na na na na na na na na na " );
				printf( "%e %e %e ", p_e[0][0] + p_o[0][0], p_e[1][0] + p_o[1][0], p_e[2][0] + p_o[2][0] );
				printf( "%e %e %e ", p_e[0][1] + p_o[0][0], p_e[1][1] + p_o[1][0], p_e[2][1] + p_o[2][0] );
				printf( "%e %e %e ", p_e[0][2] + p_o[0][0], p_e[1][2] + p_o[1][0], p_e[2][2] + p_o[2][0] );
				printf( "na na na " );
				printf( "\n" );
			}
#endif
			return 0;
		}
	}
	// error
	return -1;
}

