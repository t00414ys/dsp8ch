/*!******************************************************************************
 * @file    config_base.h
 * @brief   sensor configuration for frizz board
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __CONFIG_BASE_H__
#define __CONFIG_BASE_H__

#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)	// !EMULATION

//#
//# Accelerator /  Gyroscope
//#
/*#define USE_MPUXXXX 		// invensense				[MPU6505,MPU6500,MPU9250,MPU9255]
#define MAP_MPUXXXX 		COMPRESS_MAP(	DEFAULT_MAP_Y,				DEFAULT_MAP_X,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_BMI160			// BOSCH					[BMI160]
#define MAP_BMI160			COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_LSM330			// ST						[LSM330]
#define MAP_LSM330			COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_BMA2XX			// BOSCH(accel only)		[BMA250E,BMA255,BMA280]
#define MAP_BMA2XX			COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_MC3413		// mCube(accel only)		[MC3413]
  #define MAP_MC3413		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_STK8313		// sensortek(accel only)	[STK8313]
  #define MAP_STK8313		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
/*#define USE_ADXL362		// ANALOG DEVICES(accel only)		[ADXL362]
  #define MAP_ADXL362		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)	*/
//#
#define USE_ADS8332			// TEXAS INSTRUMENTS(accel and gyro)		[ADS8332]
  #define MAP_ADS8332		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)

#define USE_HMC5883			// HANEYWELL(gyro)		[HMC5883]
  #define MAP_HMC5883		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)

//#
//# Magnetometer
//#
//#define USE_AK09911 		// AsahiKASEI				[AK09911]
//#define USE_AK09912 		// AsahiKASEI				[AK09912]
#define MAP_AK0991X 		COMPRESS_MAP(	DEFAULT_MAP_X,				DEFAULT_MAP_Y,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_ASSERT)
//#
//#define USE_AK8963	 		// AsahiKASEI				[AK8963]
#define MAP_AK8963			COMPRESS_MAP(	DEFAULT_MAP_Y,				DEFAULT_MAP_X,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_ASSERT)
//#
//#define USE_BMM150			// BOSCH					[BMM150]
#define MAP_BMM150			COMPRESS_MAP(	DEFAULT_MAP_Y,				DEFAULT_MAP_X,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_NEGATE)
//#
//#define USE_MMC351X			// memsic					[MMC351X]
#define MAP_MMC351X			COMPRESS_MAP(	DEFAULT_MAP_Y,				DEFAULT_MAP_X,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_NEGATE)
//#
#define USE_MAG3110			// 					[MAG3110]
#define MAP_MAG3110			COMPRESS_MAP(	DEFAULT_MAP_Y,				DEFAULT_MAP_X,				DEFAULT_MAP_Z,				\
											SETTING_DIRECTION_NEGATE,	SETTING_DIRECTION_ASSERT,	SETTING_DIRECTION_NEGATE)


//#
//# Pressure
//#
#define USE_ZPA2326 		// murata					[ZPA2326]
#define USE_BME280			// BOSCH					[BME280]
#define USE_BMP280			// BOSCH					[BMP280]
#define USE_LPS25H			// ST						[LPS25H]
/*#define USE_HSPPAD031		// ALPS						[HSPPAD031]*/


//#
//# Light / Proximity
//#
#define USE_STK3420			// ST						[STK3420]
#define USE_GP2AP054A10F	// SHARP					[GP2AP054A10F]


//#
//# Optical Heart Rate Detection Sensor
//#
#define USE_LT1PH03			// murata					[LT1PH03]
#define USE_PAH8001			// PixArt					[PAH8001]
#define USE_ADPD142			// ADI						[ADPD153GGRI]


//#
//# display(OLED)
//#
#define USE_SSD1306			// SOLOMON SYSTECH			[SSD1306]
#define USE_OELD_MOCK		// dummy(Not delete!!)

#endif

#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)	// EMULATION
#define USE_ACCL_EMU
#define USE_GYRO_EMU
#define	USE_MAGN_EMU
#define	USE_PRES_EMU
#define USE_PPG_EMU
#define USE_PROX_EMU
#define USE_LIGHT_EMU
#define USE_OELD_MOCK
#endif

#endif /* __CONFIG_BASE_H__ */


